% This file tests methods for showing numerical solutions are accurate
% without necessarily knowing the exact solution

addpath('Rank1LatticeSparseFourier/algorithms');
addpath('Rank1LatticeSparseFourier/SublinearSparseFourierMATLAB');
addpath('utils');

d = 2^3;
% Diffusion coefficient
aTerms = 2;
aFreq = randi(100, aTerms, d) - 50;
aCoeff = 5 * rand(aTerms, 1)
aShift = 10;
a = gaussian(aFreq, aCoeff, aShift);

% a = @(x)cos(2*pi*x*aFreq.') * aCoeff + aShift;
% aGrad = @(x)-sin(2 * pi * x * aFreq.') * (2 * pi .* aCoeff .* aFreq);
% a_mean = 2;
% a_no_mean = [0.25, 0.125];
% a = @(x)a_mean + a_no_mean(1) .* sin(2 * pi * x * aFreq) + a_no_mean(2) * cos(2 * pi * x * (aFreq -1));
% aGrad = @(x)a_no_mean(1) * cos(2 * pi * x * aFreq) * 2 * pi * aFreq' - a_no_mean(2) * sin(2 * pi * x * (aFreq - 1)) * 2 * pi * (aFreq - 1)';
% [~, a_min] = fminsearch(a, 0.5 * ones(1, d));
% A = norm(a_no_mean, 1) / (a_min - 2 * norm(a_no_mean, 1));
% aFreqTrue = [zeros(1, d); aFreq'; -aFreq'; aFreq' - 1; -(aFreq' - 1)];
% aCoeffTrue = [a_mean; a_no_mean(1) / 2i; -a_no_mean(1) / 2i; a_no_mean(2) / 2; a_no_mean(2) / 2];

% bCell = cell(1, d);
% bFreqTrue = cell(1, d);
% bCoeffTrue = cell(1, d);
% for i = 1:d
%     bSparsity = 2;
%     bSinFreq = randi([1, 100], floor(bSparsity / 2), d) - 50;
%     [bSinFreq, sinIdx, ~] = unique(bSinFreq, 'rows');
%     bCosFreq = randi([1, 100], bSparsity - floor(bSparsity / 2), d) - 50;
%     [bCosFreq, cosIdx, ~] = unique(bCosFreq, 'rows');
% 
%     bSinCoeff = rand(floor(bSparsity / 2), 1);
%     bSinCoeff = bSinCoeff(sinIdx, :);
%     bCosCoeff = rand(bSparsity - floor(bSparsity / 2), 1);
%     bCosCoeff = bCosCoeff(cosIdx, :);
% 
%     bFreqTrue{i} = [bSinFreq; -bSinFreq; bCosFreq; -bCosFreq];
%     bCoeffTrue{i} = [bSinCoeff.' ./ 2i; -bSinCoeff.' ./ 2i; bCosCoeff.' ./ 2; bCosCoeff.' ./ 2];
%     bCell{i} = @(x)sin(2 * pi * x * bSinFreq') * bSinCoeff + cos(2 * pi * x * bCosFreq') * bCosCoeff;
% end
% 
% cSparsity = 2;
% cSinFreq = randi([1, 100], d, floor(cSparsity / 2)) - 50;
% cCosFreq = randi([1, 100], d, cSparsity - floor(cSparsity / 2)) - 50;
% cSinCoeff = rand(floor(cSparsity / 2), 1);
% cCosCoeff = rand(cSparsity - floor(cSparsity / 2), 1);
% shift = 10 * norm([cCosCoeff; cSinCoeff]);
% cFreqTrue = [zeros(1, d); cSinFreq'; -cSinFreq'; cCosFreq'; -cCosFreq'];
% cCoeffTrue = [shift; cSinCoeff.' ./ 2i; -cSinCoeff.' ./ 2i; cCosCoeff.' ./ 2; cCosCoeff.' ./ 2];
% c = @(x)sin(2 * pi * x * cSinFreq) * cSinCoeff + cos(2 * pi * x * cCosFreq) * cCosCoeff + shift;


% Forcing function
% fTheta = 7 * pi / 6;
% f = @(x)200 * pi^2 * cos(10 * pi * sum(x, 2)) .* sin(8 * pi * x * round(10 * [cos(fTheta); sin(fTheta)]));
fFreq = randi(100, d, 1) - 50;
f = @(x)cos(2*pi*x*fFreq);
fFreqTrue = [fFreq'; -fFreq'];
fCoeffTrue = [1/2; 1/2];
% f = @(x)cos(2*pi*x*fFreq) + 1;
% fFreqTrue = [fFreq'; -fFreq'; zeros(1, d)];
% fCoeffTrue = [1/2; 1/2; 1];


% Find Fourier coefficients of a and f


bandwidth = 1000;

if ~exist('primeList','var') || length(primeList) < 10000
    primeList = primes(104740); % First 10,000 primes
end

N = 2^5;
x = (0:(N - 1)) ./ N;
[xx, yy, zz] = meshgrid(x);
% [xx, yy] = meshgrid(x);


s = 100;

% Randomly construct rank-1 lattice for size s frequency set on (-N/2,
% N/2]^2 cube
sigma = 10;
threshold = max([sigma * s^2, bandwidth]) + 1;
M = min(primeList(primeList > 1000));
z = randi(M - 1, 1, d);

[fFreq, fCoeff, ~, ~] = phase_enc_fct(z, M, bandwidth, s, 'random', false, false, f, ...
    'primeList', primeList, 'C', 1, 'alpha', 1, 'beta', 1, 'primeShift', 50, 'randomScale', 1);
[aFreq, aCoeff, ~, ~] = phase_enc_fct(z, M, bandwidth, s, 'random', false, false, @a.eval, ...
    'primeList', primeList, 'C', 1, 'alpha', 1, 'beta', 1, 'primeShift', 50, 'randomScale', 1);
% bFreq = cell(1, d);
% bCoeff = cell(1, d);
% for i = 1:d
%     [bFreqTemp, bCoeffTemp, ~, ~] = phase_enc_fct(z, M, bandwidth, s, 'random', false, false, bCell{i}, ...
%         'primeList', primeList, 'C', 1, 'alpha', 1, 'beta', 1, 'primeShift', 50, 'randomScale', 1);
%     bFreq{i} = bFreqTemp;
%     bCoeff{i} = bCoeffTemp;
% end
% [cFreq, cCoeff, ~, ~] = phase_enc_fct(z, M, bandwidth, s, 'random', false, false, c, ...
%     'primeList', primeList, 'C', 1, 'alpha', 1, 'beta', 1, 'primeShift', 50, 'randomScale', 1);


coeffTol = eps;

fFreq = fFreq(abs(fCoeff) > coeffTol, :);
fCoeff = fCoeff(abs(fCoeff) > coeffTol);

aFreq = aFreq(abs(aCoeff) > coeffTol, :);
aCoeff = aCoeff(abs(aCoeff) > coeffTol);

% for i = 1:d
%     bFreq{i} = bFreq{i}(abs(bCoeff{i}) > coeffTol, :);
%     bCoeff{i} = bCoeff{i}(abs(bCoeff{i}) > coeffTol, :);
% end
% 
% cFreq = cFreq(abs(cCoeff) > coeffTol, :);
% cCoeff = cCoeff(abs(cCoeff) > coeffTol);

%%
nVals = 1:1;
gridErrors = zeros(1, length(nVals));
randErrors = zeros(1, length(nVals));
fourierErrors = zeros(1, length(nVals));


for n_idx = 1:length(nVals) % "Stamp" level
    n = nVals(n_idx)
    % Setup system
    % Construct all differences of fFreq and aFreq
    totalStamp = aFreq;
%     totalStamp = union(aFreq, cFreq, 'rows');
%     for i = 1:d
%         totalStamp = union(totalStamp, bFreq{i}, 'rows');
%     end
    k = stamp(totalStamp, fFreq, n);
    keys = num2cell(num2str(k, '%012d'), 2);
    map = containers.Map(keys, 1:length(keys));

    % Setup and solve matrix equation
    fCoeffAugment = zeros(size(k, 1), 1);
    [ink, fPos] = ismember(k, fFreq, 'rows'); % We know every element of fFreq is in k since aFreq should include 0 (a can't be mean 0)
    fCoeffAugment(ink) = fCoeff(fPos(ink));
    L = sparseDiffusionMatrix(aFreq, aCoeff, k, map);
%     L = L + sparseAdvectionMatrix(bFreq, bCoeff, k, map);
%     L = L + sparseReactionMatrix(cFreq, cCoeff, k, map);
    uCoeffTemp = L \ fCoeffAugment;
    uFreq = k; uCoeff = uCoeffTemp;
    [~, uRealPos, ~] = intersect(uFreq, -uFreq, 'rows');
    uFreq = uFreq(uRealPos, :);
    uCoeff = uCoeff(uRealPos);
    disp(['length(k) = ', num2str(length(k))])
%     clear L
%     clear k

    % Numerically compute the right hand side from the approximate solution
    recovered = @(x) exp(2i * pi .* x * uFreq') * uCoeff;
    recoveredGrad = @(x) exp(2i * pi .* x * uFreq') * (2i * pi .* uCoeff .* uFreq);
    recoveredLap = @(x) exp(2i * pi .* x * uFreq') * (-(2 * pi)^2 .* uCoeff .* sum(uFreq .* uFreq, 2));
    
%     bArr = @(x)[bCell{1}(x), bCell{2}(x), bCell{3}(x)];
%     fRecovered = @(x) a(x) .* recoveredLap(x) + sum((bArr(x) + aGrad(x)) .* recoveredGrad(x), 2) + c(x) .* recovered(x);
    fRecovered = @(x) a.eval(x) .* recoveredLap(x) + sum(a.evalGrad(x) .* recoveredGrad(x), 2);
    
    % Full grid
%     fVals = f([xx(:), yy(:), zz(:)]);
%     gridErrors(n_idx) = norm(fVals - fRecovered([xx(:), yy(:), zz(:)])) / norm(fVals);
%     fVals = f([xx(:), yy(:)]);
%     gridErrors(n_idx) = norm(fVals - fRecovered([xx(:), yy(:)])) / norm(fVals);
    % Random
    numPoints = N^2;
    points = rand(numPoints, d);
    pointsCell = num2cell(points, 1);
    fValsRand = f(points);
    randErrors(n_idx) = norm(fValsRand - fRecovered(points)) / norm(fValsRand);
end

clf
semilogy(nVals, gridErrors, '-o', 'DisplayName', ['$d=', num2str(d), '$ grid errors'])
hold on
semilogy(nVals, randErrors, '-x', 'DisplayName', ['$d=', num2str(d), '$ random errors'])

legend('show', 'Interpreter', 'latex', 'Location', 'southwest')
xlabel('$n$ (stamping level)', 'Interpreter', 'latex')
ylabel('Proxy for $\|u - u^{n,s}\|_{H^1}$', 'Interpreter', 'latex')
hold off

function k = stamp(aFreq, fFreq, n)
    d = size(aFreq, 2);
    aDif = permute(aFreq, [3, 1, 2]); % Move 2d frequencies into third dimension of tensor
    k = reshape(permute(fFreq, [1, 3, 2]) - aDif, [], 1, d); % Take advantage of implicit array expansion
    for i = 2:n
        k = reshape(k - aDif, [], 1, d);
    end
    k = unique(squeeze(k), 'rows');
    k = setdiff(k, zeros(1, d), 'rows');
end

function L = diffusionMatrix(aFreq, aCoeff, k)
    [in, aPos] = ismember(reshape(shiftdim(reshape(k', [1, size(k')]) - k, 2), [], size(aFreq, 2)), aFreq, 'rows');
    L = reshape(-(2 * pi)^2 .* (k * k'), [], 1);
    L = L .* in;
    L(in) = L(in) .* aCoeff(aPos(in));
    L = reshape(L, [], size(k, 1));
end

function L = sparseDiffusionMatrixNoHashMap(aFreq, aCoeff, k)
    val = [];
    row = [];
    col = [];
    for jIdx = 1:size(k, 1)
        j = k(jIdx, :);
        [~, ink, aPos] = intersect(j - k, aFreq, 'rows', 'stable');
        val = [val; -(2 * pi)^2 .* k(ink) * j' * aFreq(aPos)];
        row = [row; jIdx * ones(length(ink), 1)]
        col = [col; k(ink)]
    end
    L = sparse(row, col, val, size(k, 1), size(k, 1))
end

function L = sparseDiffusionMatrix(aFreq, aCoeff, k, map)
    val = [];
    row = [];
    col = [];
    parfor jIdx = 1:size(k, 1)
        j = k(jIdx, :);
        freqCell = num2cell(num2str(j - aFreq, '%012d'), 2);
        aPos = isKey(map, freqCell);
        colFreq = j - aFreq(aPos, :);
%         [~, ink, aPos] = intersect(j - k, aFreq, 'rows', 'stable');
        val = [val; -(2 * pi)^2 .* colFreq * j' .* aCoeff(aPos)];
%         -(2 * pi)^2 .* k(ink, :) * j' .* aCoeff(aPos)
        row = [row; jIdx * ones(nnz(aPos), 1)];
%         jIdx * ones(length(ink), 1)
        col = [col; cell2mat(values(map, freqCell(aPos)))];
    end
    L = sparse(row, col, val, size(k, 1), size(k, 1));
end

function L = sparseAdvectionMatrix(bFreq, bCoeff, k, map)
    val = [];
    row = [];
    col = [];
    for i = 1:length(bFreq)
        parfor jIdx = 1:size(k, 1)
            j = k(jIdx, :);
            freqCell = num2cell(num2str(j - bFreq{i}, '%012d'), 2);
            bPos = isKey(map, freqCell);
            colFreq = j - bFreq{i}(bPos, :);
            val = [val; 2i * pi * colFreq(:, i) .* bCoeff{i}(bPos)];
            row = [row; jIdx * ones(nnz(bPos), 1)];
            col = [col; cell2mat(values(map, freqCell(bPos)))];
        end
    end
    L = sparse(row, col, val, size(k, 1), size(k, 1));
end

function L = reactionMatrix(cFreq, cCoeff, k)
    [in, cPos] = ismember(reshape(shiftdim(reshape(k', [1, size(k')]) - k, 2), [], size(cFreq, 2)), cFreq, 'rows');
    L = zeros(size(k, 1)^2, 1);
%     L = L .* in;
    L(in) = cCoeff(cPos(in));
    L = reshape(L, [], size(k, 1));
end

function L = sparseReactionMatrixNoHashMap(cFreq, cCoeff, k)
    [in, cPos] = ismember(reshape(shiftdim(reshape(k', [1, size(k')]) - k, 2), [], size(cFreq, 2)), cFreq, 'rows');
    L = ones(size(k, 1)^2, 1);
    L = L .* in;
    L(in) = L(in) .* cCoeff(cPos(in));
    L = reshape(L, [], size(k, 1));
end

function L = sparseReactionMatrix(cFreq, cCoeff, k, map)
    val = [];
    row = [];
    col = [];
    parfor jIdx = 1:size(k, 1)
        j = k(jIdx, :);
        freqCell = num2cell(num2str(j - cFreq, '%012d'), 2);
        cPos = isKey(map, freqCell);
%         [~, ink, aPos] = intersect(j - k, aFreq, 'rows', 'stable');
        val = [val; cCoeff(cPos)];
%         -(2 * pi)^2 .* k(ink, :) * j' .* aCoeff(aPos)
        row = [row; jIdx * ones(nnz(cPos), 1)];
%         jIdx * ones(length(ink), 1)
        col = [col; cell2mat(values(map, freqCell(cPos)))];
    end
    L = sparse(row, col, val, size(k, 1), size(k, 1));
end