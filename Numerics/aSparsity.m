addpath('Rank1LatticeSparseFourier/algorithms');
addpath('Rank1LatticeSparseFourier/SublinearSparseFourierMATLAB');

d = 2;


%----- Code that's general in d-----%
epsilon = 1/2;
a = @(x)10 + 3 ./ (5 + 3 * sin(2 * pi * x * (1:d).' ./ epsilon)) + 3 ./ (5 + 3 * sin(2 * pi * x * (d:-1:1).' ./ epsilon)); % Multivariate version of Daubechies Testcase 1

N = 2^8;
x = (0:(N - 1)) ./ N;
c = cell(1, d);
[c{:}] = ndgrid(x);
vals = cell2mat(cellfun(@(e)e(:), c, 'UniformOutput', false));
aVals = reshape(a(vals), N * ones(1, d));
mesh(aVals)


%%
if ~exist('primeList','var') || length(primeList) < 10000
    primeList = primes(104740); % First 10,000 primes
end
sVals = 3:20;

for sIdx = 1:length(sVals)
    s = sVals(sIdx)
    bandwidth = N;
    sigma = 10;
    threshold = max([sigma * s^2, bandwidth]) + 1;
    M = min(primeList(primeList > 1000));
    z = randi(M - 1, 1, d);
    [aFreq, aCoeff, ~, ~] = phase_enc_fct(z, M, bandwidth, s, 'random', false, false, a, ...
        'primeList', primeList, 'C', 1, 'alpha', 1, 'beta', 1, 'primeShift', 50, 'randomScale', 1);
    [~, aRealPos, ~] = intersect(aFreq, -aFreq, 'rows');
    aFreq = aFreq(aRealPos, :);
    aCoeff = aCoeff(aRealPos);
end

%----- Code that's specific for d = 2 -----

afft = fftshift(fft2(aVals));
[row, col] = find(abs(afft) > 1e-6);
rowComp = row - N/2 - 1; colComp = col - N/2 - 1;
trueFreq = [rowComp, colComp]
[Lia, Locb] = ismember(trueFreq, aFreq, 'rows')


% mesh(abs(fftshift(fft2(aVals))));