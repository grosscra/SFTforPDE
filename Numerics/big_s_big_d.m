% This file tests methods for showing numerical solutions are accurate
% without necessarily knowing the exact solution

addpath('Rank1LatticeSparseFourier/algorithms');
addpath('Rank1LatticeSparseFourier/SublinearSparseFourierMATLAB');

% Diffusion coefficient
% a = @(x)1 ./ (2 + sin(20 * pi * sum(x, 2)));
% aTheta = pi / 3;
% a = @(x)1 ./ ((2 + sin(20 * pi * sum(x, 2))) .* (2 + sin(2 * pi * x * round(10 * [cos(aTheta); sin(aTheta)]))));
% a = @(x)2 * ones(size(x, 1), 1);
figure
for d = 2.^(1:2)
    
    aSparsity = 25;
    aSinFreq = randi([1, 100], d, floor(aSparsity / 2)) - 50;
    aCosFreq = randi([1, 100], d, aSparsity - floor(aSparsity / 2)) - 50;
    aSinCoeff = rand(floor(aSparsity / 2), 1);
    aCosCoeff = rand(aSparsity - floor(aSparsity / 2), 1);
    shift = 10 * norm([aCosCoeff; aSinCoeff]);
    aFreqTrue = [zeros(1, d); aSinFreq'; -aSinFreq'; aCosFreq'; -aCosFreq'];
    aCoeffTrue = [shift; aSinCoeff ./ 2i; -aSinCoeff ./ 2i; aCosCoeff ./ 2; aCosCoeff ./ 2];
    a = @(x)sin(2 * pi * x * aSinFreq) * aSinCoeff + cos(2 * pi * x * aCosFreq) * aCosCoeff + shift;

%     aFreq = randi([2, 10], d, 1);
%     numOthers = 0;
%     aFreqOthers = randi([10, 100], d, numOthers);
%     a_mean = 2;
%     a_no_mean = [0.25, 0.125];
%     aCoeffOthers = 0.15 * rand(numOthers, 1);
%     a = @(x)a_mean + a_no_mean(1) .* sin(2 * pi * x * aFreq) + a_no_mean(2) * cos(2 * pi * x * (aFreq -1)) + sin(2 * pi * x * aFreqOthers) * aCoeffOthers;
%     [~, a_min] = fminsearch(a, 0.5 * ones(1, d));
%     A = norm(a_no_mean, 1) / (a_min - 2 * norm(a_no_mean, 1));
%     aFreqTrue = [zeros(1, d); aFreq'; -aFreq'; aFreq' - 1; -(aFreq' - 1); aFreqOthers'; -aFreqOthers'];
%     aCoeffTrue = [a_mean; a_no_mean(1) / 2i; -a_no_mean(1) / 2i; a_no_mean(2) / 2; a_no_mean(2) / 2; aCoeffOthers / 2i; -aCoeffOthers / 2i];

    % Forcing function
    % fTheta = 7 * pi / 6;
    % f = @(x)200 * pi^2 * cos(10 * pi * sum(x, 2)) .* sin(8 * pi * x * round(10 * [cos(fTheta); sin(fTheta)]));
    fFreq = randi(100, d, 1) - 50;
    f = @(x)cos(2*pi*x*fFreq);
    fFreqTrue = [fFreq'; -fFreq'];
    fCoeffTrue = [1/2; 1/2];
    % f = @(x)cos(2 * pi * x * [5; 3]) - 500 * cos(2 * pi * x * [74; 76]) + 1000 * cos(2 * pi * x * [151; 149]) ;
    %.* sin(2 * pi * x * [100; 3]);


    % Find Fourier coefficients of a and f


    bandwidth = 200;

    if ~exist('primeList','var') || length(primeList) < 10000
        primeList = primes(104740); % First 10,000 primes
    end

    N = 2^5;
    % x = (0:(N - 1)) ./ N;
    % [xx, yy, zz] = meshgrid(x);


    s = 25;

    % Randomly construct rank-1 lattice for size s frequency set on (-N/2,
    % N/2]^2 cube
    sigma = 10;
    threshold = max([sigma * s^2, bandwidth]) + 1;
    M = min(primeList(primeList > 1000));
    z = randi(M - 1, 1, d);

    [fFreq, fCoeff, ~, ~] = phase_enc_fct(z, M, bandwidth, s, 'random', false, false, f, ...
        'primeList', primeList, 'C', 1, 'alpha', 1, 'beta', 1, 'primeShift', 50, 'randomScale', 1);
    [aFreq, aCoeff, ~, ~] = phase_enc_fct(z, M, bandwidth, s, 'random', false, false, a, ...
        'primeList', primeList, 'C', 1, 'alpha', 1, 'beta', 1, 'primeShift', 50, 'randomScale', 1);


    fFreq = fFreq(abs(fCoeff) > 1e-10, :); fCoeff = fCoeff(abs(fCoeff) > 1e-10);
    aFreq = aFreq(abs(aCoeff) > 1e-10, :); aCoeff = aCoeff(abs(aCoeff) > 1e-10);
    [~, fRealPos, ~] = intersect(fFreq, -fFreq, 'rows');
    fFreq = fFreq(fRealPos, :);
    fCoeff = fCoeff(fRealPos);
    [~, aRealPos, ~] = intersect(aFreq, -aFreq, 'rows');
    aFreq = aFreq(aRealPos, :);
    aCoeff = aCoeff(aRealPos);


    nVals = 1:3;
    gridErrors = zeros(1, length(nVals));
    randErrors = zeros(1, length(nVals));
    fourierErrors = zeros(1, length(nVals));


    for n_idx = 1:length(nVals) % "Stamp" level
        n = nVals(n_idx)
        % Setup system
        % Construct all differences of fFreq and aFreq
        k = stamp(aFreq, fFreq, n);
        keys = num2cell(num2str(k, '%012d'), 2);
        map = containers.Map(keys, 1:length(keys));

        % Setup and solve matrix equation
        fCoeffAugment = zeros(size(k, 1), 1);
        [ink, fPos] = ismember(k, fFreq, 'rows'); % We know every element of fFreq is in k since aFreq should include 0 (a can't be mean 0)
        fCoeffAugment(ink) = fCoeff(fPos(ink));
        L = sparseDiffusionMatrix(aFreq, aCoeff, k, map);
        uCoeffTemp = L \ fCoeffAugment;
        uFreq = k; uCoeff = uCoeffTemp;
        length(k)
%         recovered = @(x) exp(2i*pi .* x * uFreq') * uCoeff;
        clear L
        clear k
        
        
        kNext = stamp(aFreqTrue, fFreqTrue, n + 1);
        keysNext = num2cell(num2str(kNext, '%012d'), 2);
        mapNext = containers.Map(keysNext, 1:length(keysNext));
        LNext = sparseDiffusionMatrix(aFreqTrue, aCoeffTrue, kNext, mapNext);
        uCoeffAugment = zeros(size(kNext, 1), 1);
        [ink, uPos] = ismember(kNext, uFreq, 'rows');
        uCoeffAugment(ink) = uCoeff(uPos(ink));
        fApprox = LNext * uCoeffAugment;
        clear LNext
        fTrue = zeros(size(kNext, 1), 1);
        [ink, fPos] = ismember(kNext, fFreqTrue, 'rows'); % We know every element of fFreq is in k since aFreq should include 0 (a can't be mean 0)
        fTrue(ink) = fCoeffTrue(fPos(ink));
        fourierErrors(n_idx) = norm(fApprox - fTrue) ./ norm(fTrue);
        clear kNext

%         % Compute the right hand side from the approximate solution
%         z = sym('z', [1, d]);
%         recoveredSym = symfun(exp(2i * pi .* z * uFreq') * uCoeff, z);
%         aSym = symfun(a(z),z);
%         fRecoveredSym = divergence(a(z) .* gradient(recoveredSym, z), z);
%         fRecovered = matlabFunction(fRecoveredSym);
% 
%         % Full grid
%         fVals = f([xx(:), yy(:), zz(:)]);
%         gridErrors(n_idx) = norm(fVals - fRecovered(xx(:), yy(:), zz(:))) / norm(fVals);
%         % Random
%         numPoints = N;
%         points = rand(numPoints, d);
%         pointsCell = num2cell(points, 1);
%         fValsRand = f(points);
%         randErrors(n_idx) = norm(fValsRand - fRecovered(pointsCell{:})) / norm(fValsRand);

    end

    % semilogy(nVals, gridErrors)
%     semilogy(nVals, randErrors, '-x', 'DisplayName', ['$d=', num2str(d), '$ random errors'])
    semilogy(nVals, fourierErrors, '-x', 'DisplayName', ['$d=', num2str(d), '$'], 'LineWidth', 2)

    hold on
end
% semilogy(nVals, A .^(nVals + 1), 'DisplayName', '$\left(\frac{\|a - \hat a_0\|_1}{a_\mathrm{min} - 2 \|a - \hat a_0\|_1}\right)^{n + 1}$')
% A_better = norm(a_no_mean, 1) / (2 * a_min - norm(a_no_mean, 1));
% semilogy(nVals, (A_better .^(nVals + 1)) / (A_better^2 / A^2), 'DisplayName', '$\left(\frac{\|a - \hat a_0\|_1}{2 a_\mathrm{min} - \|a - \hat a_0\|_1}\right)^{n + 1}$')
% A_sqrt = sqrt(norm(a_no_mean, 1)) / (sqrt(a_min) - 2 * sqrt(norm(a_no_mean, 1)));
% semilogy(nVals, (A_sqrt .^(nVals + 1)) / (A_sqrt^2 / A^2), 'DisplayName', '$\sqrt{\frac{\|a - \hat a_0\|_1}{2 a_\mathrm{min} - \|a - \hat a_0\|_1}}^{n + 1}$')
legend('show', 'Interpreter', 'latex', 'Location', 'southwest', 'FontSize', 12)
xlabel('$n$ (stamping level)', 'Interpreter', 'latex')
ylabel('Proxy for $\|u - u^{n,s}\|_{H^1}$', 'Interpreter', 'latex')
hold off

function k = stamp(aFreq, fFreq, n)
    d = size(aFreq, 2);
    aDif = permute(aFreq, [3, 1, 2]); % Move 2d frequencies into third dimension of tensor
    k = reshape(permute(fFreq, [1, 3, 2]) - aDif, [], 1, d); % Take advantage of implicit array expansion
    for i = 2:n
        k = reshape(k - aDif, [], 1, d);
    end
    k = unique(squeeze(k), 'rows');
    k = setdiff(k, zeros(1, d), 'rows');
end

function L = diffusionMatrix(aFreq, aCoeff, k)
    [in, aPos] = ismember(reshape(shiftdim(reshape(k', [1, size(k')]) - k, 2), [], size(aFreq, 2)), aFreq, 'rows');
    L = reshape(-(2 * pi)^2 .* (k * k'), [], 1);
    L = L .* in;
    L(in) = L(in) .* aCoeff(aPos(in));
    L = reshape(L, [], size(k, 1));
end

function L = sparseDiffusionMatrix(aFreq, aCoeff, k, map)
    val = [];
    row = [];
    col = [];
    parfor jIdx = 1:size(k, 1)
        j = k(jIdx, :);
        freqCell = num2cell(num2str(j - aFreq, '%012d'), 2);
        aPos = isKey(map, freqCell);
        colFreq = j - aFreq(aPos, :);
%         [~, ink, aPos] = intersect(j - k, aFreq, 'rows', 'stable');
        val = [val; -(2 * pi)^2 .* colFreq * j' .* aCoeff(aPos)];
%         -(2 * pi)^2 .* k(ink, :) * j' .* aCoeff(aPos)
        row = [row; jIdx * ones(nnz(aPos), 1)];
%         jIdx * ones(length(ink), 1)
        col = [col; cell2mat(values(map, freqCell(aPos)))];
    end
    L = sparse(row, col, val, size(k, 1), size(k, 1));
end