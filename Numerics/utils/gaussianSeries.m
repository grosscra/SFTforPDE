classdef gaussianSeries
    %GAUSSIANSERIES Interface for a multivariate gaussian series
    %   Each gaussian can be modulated and scaled
    %TODO: Change so that gaussians are multiplied by coefficients rather
    %   than divided by coefficients squared
    
    properties
        modulation  % N x d row major array of modulations
        coeff  % N x 1 vector of scaling coefficients
        rate % N x 1 vector of coefficients to scale exponential
        M  % number of terms in infinite sum used to periodize
        shift % vertical shift to series
    end
    
    properties (SetAccess = private)
        d  % dimension of input
        periodizer  % array to facilitate periodizing gaussians by summing
    end
    
    methods (Access = public)
        function obj = gaussianSeries(modulation,coeff,rate,varargin)
            %GAUSSIANSeries Sets properties, truncates infinite sum to 10
            p = inputParser;
            p.addRequired('modulation');
            p.addRequired('coeff');
            p.addRequired('rate');
            p.addParameter('shift', 0);
            p.addParameter('M', 10);  % Seems to work well in practice
            p.parse(modulation, coeff, rate, varargin{:})
            r = p.Results;
            obj.modulation = r.modulation;
            obj.d = size(modulation, 2);
            obj.coeff = r.coeff;
            obj.rate = abs(r.rate); % Since squared, sign doesn't matter
            obj.shift = r.shift;
            obj.M = r.M;
            reshapeDims = [1, 1, 2 * obj.M + 1];
            obj.periodizer = reshape(-obj.M:obj.M, reshapeDims);
        end
        
        function y = eval(obj,x)
            %EVAL Evaluate gaussian series on row-major array of points
            y = real(obj.series(@obj.termEval, x) + obj.shift);
        end
        
        function y = evalGrad(obj,x)
            %EVALGRAD Evaluate gradient on row-major array of points
            y = real(obj.series(@obj.termGradEval, x));
        end
        
        function c = plus(a, b)
            if isa(b, "double")
                c = gaussianSeries(a.modulation, a.coeff, a.rate, ...
                    'shift', b + a.shift, 'M', a.M);
            elseif isa(b, "gaussianSeries")
                if isa(a, "gaussianSeries")
                    aModRate = [a.modulation, a.rate];
                    bModRate = [b.modulation, b.rate];
                    newModRate = union(aModRate, bModRate, 'rows');
                    newCoeff = zeros(size(newModRate, 1), 1);
                    [aPart, aIdx] = ismember(newModRate, aModRate, 'rows');
                    newCoeff(aPart) = newCoeff(aPart) ...
                        + a.coeff(nonzeros(aIdx));
                    [bPart, bIdx] = ismember(newModRate, bModRate, 'rows');
                    newCoeff(bPart) = newCoeff(bPart) ...
                        + b.coeff(nonzeros(bIdx));
                    newMod = newModRate(:, 1:(end - 1));
                    newRate = newModRate(:, end);
                    c = gaussianSeries(newMod, newCoeff, newRate,...
                        'shift', a.shift + b.shift, 'M', max(a.M, b.M)); 
                else
                    c = b + a;
                end
            else
                errMsg = ['Cannot add gaussianSeries with type ', ...
                    class(b)];
                ME = MException('gaussianSeries:typeException', ...
                    errMsg, str);
                throw(ME);
            end 
        end
        
        function c = times(a, b)
            if isa(a, "double")
                c = gaussianSeries(b.modulation, a .* b.coeff, b.rate, ...
                    'shift', a.* b.shift, 'M', b.M);
            else
                c = b .* a;
            end
        end
        
        function b = uminus(a)
            b = -1 .* a;
        end
        
        function c = minus(a, b)
            c = a + -b;
        end
        
    end
    
    methods (Access = private)
        
        function y = series(obj, func, x)
            %SERIES Sums all function of each term applied to input
            y = zeros(size(x, 1), 1);
            for i = 1:size(obj.modulation, 1)
                y = y + func(x, obj.modulation(i, :), obj.coeff(i),...
                    obj.rate(i));
            end
        end
        
        function y = termEval(obj, x, modulation, c, rate)
            %TERMEVAL Evaluates one term in the gaussian series
            modulator = exp(2i .* pi .* modulation .* x);
            y = (c * sqrt(2 * pi) ^ obj.d / (rate ^ obj.d)) .* prod(modulator .*...
                sum(...
                    exp(-2 .* (pi .* (mod(x, 1) - obj.periodizer) ./ rate).^2 ...
                    ) ...
                    , 3 ...
                ), 2);

        end
        
        function y = termGradEval(obj, x, modulation, c, rate)
            %TERMGRADEVAL Evaluates one gradient in the gaussian series
            modulator = exp(2i .* pi .* modulation .* x);
            y = (2i * pi * modulation) .* ...
                obj.termEval(x, modulation, c, rate);
            for i = 1:obj.d
                difference = mod(x, 1) - obj.periodizer;
                maskSize = [size(x), 2 * obj.M + 1];
                mask = ones(maskSize);
                mask(:, i, :) = -(2 * pi ./ rate)^2 .* difference(:, i, :);
                y(:, i) = y(:, i) + real(prod(modulator .*...
                sum(...
                    mask .* exp(-2 .* (pi .* (mod(x, 1) - obj.periodizer) / rate).^2 ...
                    ) ...
                    , 3 ...
                ), 2));
            end
        end
    end
end

