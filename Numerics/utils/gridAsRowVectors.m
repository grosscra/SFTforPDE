function x = gridAsRowVectors(d,N)
%gridAsRowVectors Create a length N (in each dimension) d-dimensional grid
%   Each point in the grid is a row vector.
% NOTE: Only works for small grids

nodes = (0:(N - 1)) ./ N; % One dimension of grid points
c = cell(1, d); % Get cell array of proper size for ndgrid
[c{:}] = ndgrid(nodes);
x = cell2mat(cellfun(@(e)e(:), c, 'UniformOutput', false));

end

