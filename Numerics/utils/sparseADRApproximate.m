classdef sparseADRApproximate <  sparseADR
    %sparseADRApproximate Sparse ADR on data given as function handles
    %These functions will be approximated using a sparse Fourier transform
    %
    % NOTE: Sparse Fourier transform default parameters are chosen to 
    % balance speed and accuracy, enough so that random draws fail 
    % frequently enough to notice. 
    
    properties
        f
        a
        b
        c
        aGrad
    end
    
    methods
        function obj = sparseADRApproximate(d,s,N,f,a,varargin)
            %sparseADRApproximate

            addpath('../Rank1LatticeSparseFourier/algorithms', ...
            '../Rank1LatticeSparseFourier/SublinearSparseFourierMATLAB');
            p = inputParser;
            p.CaseSensitive = true;
            p.addRequired('d');
            p.addRequired('s');
            p.addRequired('N');
            p.addRequired('f');
            p.addRequired('a');
            p.addParameter('b', {@(x)0});
            p.addParameter('c', @(x)0);
            p.addParameter('sparseMatrix', true);
            p.addParameter('coeffTol', eps);
            p.addParameter('sigma', 0.1);
            p.addParameter('aGrad', @(x)0);
            p.addParameter('C', 0.1);
            p.addParameter('primeShift', 50);
            p.addParameter('randomScale', 1/2);
            p.parse(d,s,N,f,a,varargin{:});
            r = p.Results;
            [z, M] = sparseADRApproximate.generateRank1Lattice(r.d, r.s,...
                r.N, r.sigma);
            [fFreq, fCoeff, ~, ~] = phase_enc_fct(z, M, r.N, r.s, ...
                'random', false, false, r.f, ...
                'primeList', sparseADRApproximate.getPrimeList, 'C', r.C,...
                'primeShift', r.primeShift, 'randomScale', r.randomScale);
            [aFreq, aCoeff, ~, ~] = phase_enc_fct(z, M, r.N, r.s, ...
                'random', false, false, r.a, ...
                'primeList', sparseADRApproximate.getPrimeList, 'C', r.C, ...
                'primeShift', r.primeShift, 'randomScale', r.randomScale);
            sort(fCoeff);
            superArgs = {fFreq; fCoeff; aFreq; aCoeff};
            if ~any(strcmp(p.UsingDefaults, 'b'))
                bFreq = cell(1, d);
                bCoeff = cell(1, d);
                for i = 1:d
                    [bFreqTemp, bCoeffTemp, ~, ~] = phase_enc_fct(z, M, ...
                        r.N, r.s, 'random', false, false, r.b{i}, ...
                        'primeList', sparseADRApproximate.getPrimeList, ...
                        'C', r.C, 'primeShift', r.primeShift,...
                        'randomScale', r.randomScale);
                    bFreq{i} = bFreqTemp;
                    bCoeff{i} = bCoeffTemp;
                end
                superArgs = [superArgs; 'bFreq'; {bFreq}; ...
                    'bCoeff'; {bCoeff}];
            end
            if ~any(strcmp(p.UsingDefaults, 'c'))
                [cFreq, cCoeff, ~, ~] = phase_enc_fct(z, M, r.N, r.s, ...
                    'random', false, false, r.c, ...
                    'primeList', sparseADRApproximate.getPrimeList, ...
                    'C', r.C, 'primeShift', r.primeShift, ...
                    'randomScale', r.randomScale);
                superArgs = [superArgs; 'cFreq'; cFreq; 'cCoeff'; cCoeff];

            end
            superArgs = [superArgs; 'sparseMatrix'; r.sparseMatrix; ...
                'coeffTol'; r.coeffTol];
            obj@sparseADR(superArgs{:});
            obj.f = r.f;
            obj.a = r.a;
            if obj.isAdvection
                obj.b = r.b;
            end
            if obj.isReaction
                obj.c = r.c;
            end
            if ~any(strcmp(p.UsingDefaults, 'aGrad'))
                obj.aGrad = r.aGrad;
            end
        end
        
        function Lu = apply(obj,u)
            % u must be a Fourier series object
            % Returns a function handle
            fDiffusion = @(x) obj.a(x) .* u.evalLaplacian(x) +...
                sum(obj.aGrad(x) .* u.evalGrad(x), 2);
            if obj.isAdvection
                % Need to convert b from a cell array of function handles to a
                % vector-valued function handle. (This is potentially the worst
                % two lines of MATLAB code I can imagine).
                fCellApplier = @(x)cellfun(@(f)f(x), obj.b);
                % arrayfun needs to separate out columns to input into function,
                % whereas fCellApplier takes in one input as a row vector. So 
                % instead, we have to split the input into a cell array using
                % mat2cell, then apply cellfun, then send back to matrix using
                % cell2mat.
                bArr = @(x) cell2mat(...
                                cellfun(fCellApplier, ...
                                        mat2cell(x, ones(1, size(x, 1))),...
                                        'UniformOutput', false...
                                )...
                            );

                fAdvection = @(x) sum(bArr(x) .* u.evalGrad(x), 2);
            else
                fAdvection = @(x)0;
            end
            if obj.isReaction
                fReaction = @(x) obj.c(x) .* u.eval(x);
            else
                fReaction = @(x)0;
            end
            Lu = @(x) fDiffusion(x) + fAdvection(x) + fReaction(x);
        end
    end
    
    methods (Static)
        function out = getPrimeList(varargin)
            persistent primeList
            if ~isempty(varargin) && varargin{1} > 104740
                primeList = primes(varargin{1});
            else
                if length(primeList) < 10000
                    primeList = primes(104740); % First 10,000 primes
                end
            end
            out = primeList;
        end
        
        function [z, M] = generateRank1Lattice(d, s, N, sigma)
            % Generate primes for lattice and phase_enc
            threshold = max([s^2 / sigma, N]) + 1;
            primeList = sparseADRApproximate.getPrimeList(2 * threshold);
            M = min(primeList(primeList >= threshold));
            z = randi(M - 1, 1, d);
        end
    end
end

