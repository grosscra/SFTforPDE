function y = trueUnivariateSolutionParallel(a, f, x)
    % Compute the solution u of  (a u')' = f at the values of x in parallel
    % a and f are function handles of 1d functions
    % x is an array of points to evaluate u on
    intermediate = @(t)integral(f, 0, t) ./ a(t);
    intermediateArr = @(t)arrayfun(intermediate, t);
    solution = @(x)integral(intermediateArr, 0, x);
    y = zeros(1, numel(x));
    parfor i = 1:numel(x)
        y(i) = solution(x(i));
    end
    y = reshape(y, size(x));
end 