classdef fourierSeriesSine < fourierSeries
    %FOURIERSINE Interface for a multivariate Fourier sine series
    
    methods (Access = public)
        function obj = fourierSeriesSine(freq,coeff)
            %FOURIER Sets properties
            freqComplex = [freq; -freq];
            coeffComplex = [coeff ./ 2i; - coeff ./ 2i];
            obj@fourierSeries(freqComplex, coeffComplex, 'forceReal', true);
        end 
    end
end

