function [u, fApprox] = solveSparseADR(d,s,n,N,f,a,varargin)
%solveSparseADR Solve the ADR equation with sparse data
%
%
% Syntax: [u, ...] = solveSparseADR(d, s, n, N, f, a, ...)
%
% Inputs:
%   d - Dimension
%   s - Fourier-sparsity level of input data
%   n - Stamping level
%   N - Maximum bandwidth of data in any dimension
%   f - Function handle for right hand side
%   a - Function handle for diffusion coefficient
% Optional inputs:
%   b - 1 x d cell-array of function handles for advection term
%   c - Function handle for reaction term
%   aGrad - Function handle for gradient of diffusion coefficient
% Name-value pairs:
%   'sigma' - Probability of failure for rank-1 lattice generation
%       (default: 0.1)
%   'coefTol' - Tolerance for Fourier coefficient thresholding of data
%       (default: eps)
%   'applyOperator' - Boolean switch for return approximate solution passed
%       through ADR operator in fApprox. Note that aGrad must be passed as
%       input (default: false)
%   
% Outputs:
%   u - Fourier series object of solution. See fourierSeries for details.
% Optional outputs: 
%   fApprox - Function handle of u passed through ADR operator. Only
%       returned if 'applyOperator' is true, otherwise, will pass constant
%       infinity function.
%
% NOTE: Sparse Fourier transform parameters are hardcoded (with sensible
%   defaults). This should be changed in the future.

addpath('../Rank1LatticeSparseFourier/algorithms', ...
    '../Rank1LatticeSparseFourier/SublinearSparseFourierMATLAB');

p = inputParser;
p.CaseSensitive = true;
p.addRequired('d');
p.addRequired('s');
p.addRequired('n');
p.addRequired('N');
p.addRequired('f');
p.addRequired('a');
p.addOptional('b', {@(x)0});
p.addOptional('c', @(x)0);
p.addParameter('aGrad', @(x)0);
p.addParameter('sigma', .1);
p.addParameter('coeffTol', eps);
p.addParameter('applyOperator', false);
p.parse(d,s,n,N,f,a,varargin{:});
r = p.Results;

% Check whether advection and reaction terms were provided
isAdvection = ~any(strcmp(p.UsingDefaults, 'b'));
isReaction = ~any(strcmp(p.UsingDefaults, 'c'));

% Generate primes for lattice and phase_enc
if ~exist('primeList','var') || length(primeList) < 10000
    primeList = primes(104740); % First 10,000 primes
end

[z, M] = generateRank1Lattice(r.d, r.s, r.N, r.sigma, primeList);

% Get Fourier data of PDE data
[fFreq, fCoeff, ~, ~] = phase_enc_fct(z, M, r.N, r.s, 'random', ...
    false, false, r.f, 'primeList', primeList, 'C', 1, 'alpha', 1, ...,
    'beta', 1, 'primeShift', 50, 'randomScale', 1);
[fFreq, fCoeff] = threshold(fFreq, fCoeff, r.coeffTol);

[aFreq, aCoeff, ~, ~] = phase_enc_fct(z, M, r.N, r.s, 'random', ...
    false, false, r.a, 'primeList', primeList, 'C', 1, 'alpha', 1, ...,
    'beta', 1, 'primeShift', 50, 'randomScale', 1);
[aFreq, aCoeff] = threshold(aFreq, aCoeff, r.coeffTol);
totalStamp = aFreq;

if isAdvection
    bFreq = cell(1, d);
    bCoeff = cell(1, d);
    for i = 1:d
        [bFreqTemp, bCoeffTemp, ~, ~] = phase_enc_fct(z, M, bandwidth, ...
            r.s, 'random', false, false, r.b{i}, 'primeList', primeList,...
            'C', 1, 'alpha', 1, 'beta', 1, 'primeShift', 50, ...
            'randomScale', 1);
        [bFreqTemp, bCoeffTemp] = threshold(bFreqTemp, bCoeffTemp, ...
            r.coeffTol);
        bFreq{i} = bFreqTemp;
        bCoeff{i} = bCoeffTemp;
        totalStamp = union(totalStamp, bFreqTemp, 'rows');
    end
end

if isReaction
    [cFreq, cCoeff, ~, ~] = phase_enc_fct(z, M, bandwidth, r.s, ...
        'random', false, false, r.c, 'primeList', primeList, 'C', 1, ...
        'alpha', 1, 'beta', 1, 'primeShift', 50, 'randomScale', 1);
    [cFreq, cCoeff] = threshold(cFreq, cCoeff, r.coeffTol);
    totalStamp = union(cFreq, 'rows');
end

% Create full stamping set
uFreq = stamp(totalStamp, fFreq, r.n);
% Hacky way to map all frequencies in the stamp set to their matrix index
keys = num2cell(num2str(uFreq, '%012d'), 2);
map = containers.Map(keys, 1:length(keys));

% Setup and solve matrix equation
fCoeffAugment = zeros(size(uFreq, 1), 1);
% We know every element of fFreq is in uFreq since aFreq should include the
%   zero frequency (since a cannot be mean zero).
[inuFreq, fPos] = ismember(uFreq, fFreq, 'rows');
fCoeffAugment(inuFreq) = fCoeff(fPos(inuFreq));
L = sparseDiffusionMatrix(aFreq, aCoeff, uFreq, map);
if isAdvection
    L = L + sparseAdvectionMatrix(bFreq, bCoeff, uFreq, map);
end
if isReaction
    L = L + sparseReactionMatrix(cFreq, cCoeff, uFreq, map);
end
uCoeff = L \ fCoeffAugment;
[uFreq, uCoeff] = forceReal(uFreq, uCoeff);
u = fourierSeries(uFreq, uCoeff, 'forceReal', true);

if r.applyOperator
    fDiffusion = @(x) r.a(x) .* u.evalLaplacian(x) +...
        sum(r.aGrad(x) .* u.evalGrad(x), 2);
    if isAdvection
        % Need to convert b from a cell array of function handles to a
        % vector-valued function handle. (This is potentially the worst
        % two lines of of MATLAB code I can imagine).
        fCellApplier = @(x)cellfun(@(f)f(x), f);
        % arrayfun needs to separate out columns to input into function,
        % whereas fCellApplier takes in one input as a row vector. So 
        % instead, we have to split the input into a cell array using
        % mat2cell, then apply cellfun, then send back to matrix using
        % cell2mat.
        bArr = @(x) cell2mat(...
                        cellfun(fCellApplier, ...
                                mat2cell(x, ones(1, size(x, 1))),...
                                'UniformOutput', false...
                        )...
                    );
        
        fAdvection = sum(bArr(x) .* u.evalGrad(x), 2);
    else
        fAdvection = @(x)0;
    end
    if isReaction
        fReaction = @(x) r.c(x) .* u.eval(x);
    else
        fReaction = @(x)0;
    end
    fApprox = @(x) fDiffusion(x) + fAdvection(x) + fReaction(x);

else
    fApprox = @(x)Inf;
end

end

function [z, M] = generateRank1Lattice(d, s, N, sigma, primeList)
    threshold = max([s^2 / sigma, N]) + 1;
    M = min(primeList(primeList > threshold));
    z = randi(M - 1, 1, d);
end

function [freq, coeff] = threshold(freq, coeff, coeffTol)
    freq = freq(abs(coeff) > coeffTol, :);
    coeff = coeff(abs(coeff) > coeffTol);
end

function k = stamp(aFreq, fFreq, n)
    d = size(aFreq, 2);
    aDif = permute(aFreq, [3, 1, 2]); % Move 2d frequencies into third dimension of tensor
    k = reshape(permute(fFreq, [1, 3, 2]) - aDif, [], 1, d); % Take advantage of implicit array expansion
    for i = 2:n
        k = reshape(k - aDif, [], 1, d);
    end
    k = unique(squeeze(k), 'rows');
    k = setdiff(k, zeros(1, d), 'rows');
end

% function L = diffusionMatrix(aFreq, aCoeff, k)
%     [in, aPos] = ismember(reshape(shiftdim(reshape(k', [1, size(k')]) - k, 2), [], size(aFreq, 2)), aFreq, 'rows');
%     L = reshape(-(2 * pi)^2 .* (k * k'), [], 1);
%     L = L .* in;
%     L(in) = L(in) .* aCoeff(aPos(in));
%     L = reshape(L, [], size(k, 1));
% end

% function L = sparseDiffusionMatrixNoHashMap(aFreq, aCoeff, k)
%     val = [];
%     row = [];
%     col = [];
%     for jIdx = 1:size(k, 1)
%         j = k(jIdx, :);
%         [~, ink, aPos] = intersect(j - k, aFreq, 'rows', 'stable');
%         val = [val; -(2 * pi)^2 .* k(ink) * j' * aFreq(aPos)];
%         row = [row; jIdx * ones(length(ink), 1)]
%         col = [col; k(ink)]
%     end
%     L = sparse(row, col, val, size(k, 1), size(k, 1))
% end

function L = sparseDiffusionMatrix(aFreq, aCoeff, k, map)
    val = [];
    row = [];
    col = [];
    parfor jIdx = 1:size(k, 1)
        j = k(jIdx, :);
        freqCell = num2cell(num2str(j - aFreq, '%012d'), 2);
        aPos = isKey(map, freqCell);
        colFreq = j - aFreq(aPos, :);
%         [~, ink, aPos] = intersect(j - k, aFreq, 'rows', 'stable');
        val = [val; -(2 * pi)^2 .* colFreq * j' .* aCoeff(aPos)];
%         -(2 * pi)^2 .* k(ink, :) * j' .* aCoeff(aPos)
        row = [row; jIdx * ones(nnz(aPos), 1)];
%         jIdx * ones(length(ink), 1)
        col = [col; cell2mat(values(map, freqCell(aPos)))];
    end
    L = sparse(row, col, val, size(k, 1), size(k, 1));
end

function L = sparseAdvectionMatrix(bFreq, bCoeff, k, map)
    val = [];
    row = [];
    col = [];
    for i = 1:length(bFreq)
        parfor jIdx = 1:size(k, 1)
            j = k(jIdx, :);
            freqCell = num2cell(num2str(j - bFreq{i}, '%012d'), 2);
            bPos = isKey(map, freqCell);
            colFreq = j - bFreq{i}(bPos, :);
            val = [val; 2i * pi * colFreq(:, i) .* bCoeff{i}(bPos)];
            row = [row; jIdx * ones(nnz(bPos), 1)];
            col = [col; cell2mat(values(map, freqCell(bPos)))];
        end
    end
    L = sparse(row, col, val, size(k, 1), size(k, 1));
end

% function L = reactionMatrix(cFreq, cCoeff, k)
%     [in, cPos] = ismember(reshape(shiftdim(reshape(k', [1, size(k')]) - k, 2), [], size(cFreq, 2)), cFreq, 'rows');
%     L = zeros(size(k, 1)^2, 1);
% %     L = L .* in;
%     L(in) = cCoeff(cPos(in));
%     L = reshape(L, [], size(k, 1));
% end

% function L = sparseReactionMatrixNoHashMap(cFreq, cCoeff, k)
%     [in, cPos] = ismember(reshape(shiftdim(reshape(k', [1, size(k')]) - k, 2), [], size(cFreq, 2)), cFreq, 'rows');
%     L = ones(size(k, 1)^2, 1);
%     L = L .* in;
%     L(in) = L(in) .* cCoeff(cPos(in));
%     L = reshape(L, [], size(k, 1));
% end

function L = sparseReactionMatrix(cFreq, cCoeff, k, map)
    val = [];
    row = [];
    col = [];
    parfor jIdx = 1:size(k, 1)
        j = k(jIdx, :);
        freqCell = num2cell(num2str(j - cFreq, '%012d'), 2);
        cPos = isKey(map, freqCell);
        val = [val; cCoeff(cPos)];
        row = [row; jIdx * ones(nnz(cPos), 1)];
        col = [col; cell2mat(values(map, freqCell(cPos)))];
    end
    L = sparse(row, col, val, size(k, 1), size(k, 1));
end

function [freq, coeff] = forceReal(freq, coeff)
    [~, realPos, ~] = intersect(freq, -freq, 'rows');
    freq = freq(realPos, :);
    coeff = coeff(realPos, :);
end
