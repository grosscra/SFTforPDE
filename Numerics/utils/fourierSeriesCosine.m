classdef fourierSeriesCosine < fourierSeries
    %FOURIERCOSINE Interface for a multivariate Fourier cosine series
    
    methods (Access = public)
        function obj = fourierSeriesCosine(freq,coeff)
            %FOURIER Sets properties
            [meanIdx, ~] = ismember(freq, zeros(1, size(freq, 2)), 'rows');
            
            if any(meanIdx)
                mean = coeff(meanIdx);
                freq = freq(~meanIdx, :);
                coeff = coeff(~meanIdx);
            end
            freqComplex = [freq; -freq];
            coeffComplex = [coeff ./ 2; coeff ./ 2];
            if any(meanIdx)
                freqComplex = [zeros(1, size(freq, 2)); freqComplex];
                coeffComplex = [mean; coeffComplex];
            end
            obj@fourierSeries(freqComplex, coeffComplex, 'forceReal', true);
        end 
    end
end