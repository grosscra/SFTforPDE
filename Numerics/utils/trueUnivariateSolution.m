function y = trueUnivariateSolution(a, f, x)
    % Compute the solution u of  (a u')' = f at the values of x
    % a and f are function handles of 1d functions
    % x is an array of points to evaluate u on
    intermediate = @(t)integral(f, 0, t) ./ a(t);
    intermediateArr = @(t)arrayfun(intermediate, t);
    solution = @(x)integral(intermediateArr, 0, x);
    y = arrayfun(solution, x);
end 