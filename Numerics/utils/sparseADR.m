classdef sparseADR < handle
    %UNTITLED2 Summary of this class goes here
    %   Detailed explanation goes here
    
    properties (SetAccess = private)
        fFreq
        fCoeff
        aFreq
        aCoeff
        isAdvection
        bFreq
        bCoeff
        isReaction
        cFreq
        cCoeff
        totalStamp
        sparseMatrix
        coeffTol
    end % properties
    
    methods (Access = public)
        function obj = sparseADR(fFreq, fCoeff, aFreq, aCoeff, varargin)
            %sparseADR ADR equation from Fourier coefficients of data
            p = inputParser;
            p.addRequired('fFreq');
            p.addRequired('fCoeff');
            p.addRequired('aFreq');
            p.addRequired('aCoeff');
            p.addParameter('bFreq', {[]});
            p.addParameter('bCoeff', {[]});
            p.addParameter('cFreq', []);
            p.addParameter('cCoeff', []);
            p.addParameter('sparseMatrix', true);
            p.addParameter('coeffTol', eps);
            p.parse(fFreq, fCoeff, aFreq, aCoeff, varargin{:});
            obj.setProperties(p);
        end

        function k = stamp(obj, n)
            d = size(obj.totalStamp, 2);
            % Move 2d frequencies into third dimension of tensor
            stampDiff = permute(obj.totalStamp, [3, 1, 2]); 
             % Take advantage of implicit array expansion
            k = reshape(permute(obj.fFreq, [1, 3, 2]) - stampDiff,...
                        [], 1, d);
            for i = 2:n
                k = reshape(k - stampDiff, [], 1, d);
            end
            k = unique(squeeze(k), 'rows');
            if ~obj.isAdvection || ~obj.isReaction
                % Diffusion only is determined up to constant shifts
                k = setdiff(k, zeros(1, d), 'rows');
            end
        end

        function L = assembleOperator(obj, freq)
            if obj.sparseMatrix
                % Hacky way to map all frequencies in the stamp set to
                %   their matrix indices
                keys = num2cell(num2str(freq, '%012d'), 2);
                map = containers.Map(keys, 1:length(keys));
                L = obj.sparseDiffusionMatrix(freq, map);
                if obj.isAdvection
                    L = L + obj.sparseAdvectionMatrix(freq, map);
                end
                if obj.isReaction
                    L = L + obj.sparseReactionMatrix(freq, map);
                end
            else
                L = obj.diffusionMatrix(freq);
                % No non-sparse advection matrix code available
                if obj.isReaction
                    L = L + obj.reactionMatrix(freq);
                end
            end
        end
        
        function u = solve(obj,n)
            % Create full stamping set
            freq = obj.stamp(n);
            L = obj.assembleOperator(freq);
            fCoeffAugment = zeros(size(freq, 1), 1);
            % We know every element of fFreq is in freq since aFreq should 
            %   include the zero frequency (since a cannot be mean zero).
            [inFreq, fPos] = ismember(freq, obj.fFreq, 'rows');
            fCoeffAugment(inFreq) = obj.fCoeff(fPos(inFreq));
            coeff = L \ fCoeffAugment;
            u = fourierSeries(freq, coeff, 'forceReal', true);
        end
        
        function Lu = applyToFourierSparse(obj, u, n)
            % u must be a Fourier series object supported on stamp set n
            % Returns a Fourier series object
            % NOTE: u MUST be supported on the nth stamping set, since this 
            % computes a Fourier series object supported on the (n + 1) 
            % stamping set! See Proposition 5 for details.
            freq = obj.stamp(n + 1);
            L = obj.assembleOperator(freq);
            uCoeffAugment = zeros(size(freq, 1), 1);
            [inFreq, uPos] = ismember(freq, u.freq, 'rows');
            uCoeffAugment(inFreq) = u.coeff(uPos(inFreq));
            coeff = L * uCoeffAugment;
            Lu = fourierSeries(freq, coeff);
        end
    end % public methods
    
    methods (Access = private)
        function [freq, coeff] = threshold(obj, freq, coeff)
            freq = freq(abs(coeff) > obj.coeffTol, :);
            coeff = coeff(abs(coeff) > obj.coeffTol);

            % This part forces the returned coefficients to be that of a
            % real valued function (so it doesn't actually do thresholding
            % and should be moved to it's own function: TODO)
            newFreq = union(freq, -freq, 'rows');
            newCoeff = zeros(size(newFreq, 1), 1);
            [aPart, aIdx] = ismember(newFreq, freq, 'rows');
            newCoeff(aPart) = newCoeff(aPart) ...
                + coeff(nonzeros(aIdx));
            [bPart, bIdx] = ismember(newFreq, -freq, 'rows');
            newCoeff(bPart) = newCoeff(bPart) ...
                + conj(coeff(nonzeros(bIdx)));
            freq = newFreq;
            coeff = newCoeff / 2;
        end
        
        function setProperties(obj, p)
            r = p.Results;
            obj.coeffTol = r.coeffTol;
            [obj.aFreq, obj.aCoeff] = obj.threshold(r.aFreq, r.aCoeff);
            [obj.fFreq, obj.fCoeff] = obj.threshold(r.fFreq, r.fCoeff);
            obj.sparseMatrix = r.sparseMatrix;
            
            % Check whether advection and reaction terms were provided
            obj.isAdvection = ~any(strcmp(p.UsingDefaults, 'bFreq'));
            obj.isReaction = ~any(strcmp(p.UsingDefaults, 'cFreq'));
            
            obj.totalStamp = obj.aFreq;
            if obj.isAdvection
                obj.bFreq = cell(1, length(r.bFreq));
                obj.bCoeff = cell(1, length(r.bCoeff));
                for i = 1:length(r.bFreq)
                    [obj.bFreq{i}, obj.bCoeff{i}] = obj.threshold(...
                        r.bFreq{i}, r.bCoeff{i});
                    obj.totalStamp = union(obj.totalStamp, obj.bFreq{i},...
                        'rows');
                end
                % No non-sparse advection matrix code available
                obj.sparseMatrix = true;
            end
            if obj.isReaction
                [obj.cFreq, obj.cCoeff] = obj.threshold(r.cFreq, r.cCoeff);
                obj.totalStamp = union(obj.totalStamp, r.cFreq, 'rows');
            end
        end
        
        function L = sparseDiffusionMatrix(obj, freq, map)
            val = [];
            row = [];
            col = [];
            parfor jIdx = 1:size(freq, 1)
                j = freq(jIdx, :);
                freqCell = num2cell(num2str(j - obj.aFreq, '%012d'), 2);
                aPos = isKey(map, freqCell);
                colFreq = j - obj.aFreq(aPos, :);
                val = [val;-(2*pi)^2 .* colFreq * j' .* obj.aCoeff(aPos)];
                row = [row; jIdx * ones(nnz(aPos), 1)];
                col = [col; cell2mat(values(map, freqCell(aPos)))];
            end
            L = sparse(row, col, val, size(freq, 1), size(freq, 1));
        end
        
        function L = diffusionMatrix(obj, freq)
            % This is a really terrible (but MATLAB optimized-ish) way to
            % get the convolution type structure of the Fourier
            % coefficients of a into the matrix. See sparseDiffusionMatrix
            % above for the more readable version of what's going on.
            [in, aPos] =    ismember(...
                                reshape(...
                                    shiftdim(...
                                        reshape(freq', [1, size(freq')])...
                                        - freq, 2 ...
                                    ),...
                                    [], size(obj.aFreq, 2)...
                                ),...
                            obj.aFreq, 'rows');
            L = reshape(-(2 * pi)^2 .* (freq * freq'), [], 1);
            L = L .* in;
            L(in) = L(in) .* obj.aCoeff(aPos(in));
            L = reshape(L, [], size(freq, 1));
        end
        
        function L = sparseAdvectionMatrix(obj, freq, map)
            val = [];
            row = [];
            col = [];
            for i = 1:length(obj.bFreq)
                parfor jIdx = 1:size(freq, 1)
                    j = freq(jIdx, :);
                    freqCell = num2cell(...
                        num2str(j - obj.bFreq{i}, '%012d'), 2);
                    bPos = isKey(map, freqCell);
                    colFreq = j - obj.bFreq{i}(bPos, :);
                    val = [val;2i*pi*colFreq(:, i).*obj.bCoeff{i}(bPos)];
                    row = [row; jIdx * ones(nnz(bPos), 1)];
                    col = [col; cell2mat(values(map, freqCell(bPos)))];
                end
            end
            L = sparse(row, col, val, size(freq, 1), size(freq, 1));
        end
        
        function L = sparseReactionMatrix(obj, freq, map)
            val = [];
            row = [];
            col = [];
            parfor jIdx = 1:size(freq, 1)
                j = freq(jIdx, :);
                freqCell = num2cell(num2str(j - obj.cFreq, '%012d'), 2);
                cPos = isKey(map, freqCell);
                val = [val; obj.cCoeff(cPos)];
                row = [row; jIdx * ones(nnz(cPos), 1)];
                col = [col; cell2mat(values(map, freqCell(cPos)))];
            end
            L = sparse(row, col, val, size(freq, 1), size(freq, 1));
        end
        
        function L = reactionMatrix(obj, freq)
            [in, cPos] =    ismember(...
                                reshape(...
                                    shiftdim(...
                                        reshape(freq', [1, size(freq')])...
                                        - freq, 2 ...
                                    ), [], size(obj.cFreq, 2) ...
                                ), obj.cFreq, 'rows' ...
                            );
            L = zeros(size(k, 1)^2, 1);
            L(in) = obj.cCoeff(cPos(in));
            L = reshape(L, [], size(freq, 1));
        end
    end % private methods
end % classdef

