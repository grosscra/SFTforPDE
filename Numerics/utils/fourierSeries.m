classdef fourierSeries
    %FOURIER Interface for a multivariate (complex) Fourier series
    
    properties
        freq  % N x d row-major array of frequencies
        coeff  % N x 1 vector of coefficients
        forceReal % Boolean forcing series to be real-valued (default false)
    end
    
    properties (SetAccess = private)
        d  % dimension of input
    end
    
    methods (Access = public)
        function obj = fourierSeries(freq,coeff,varargin)
            %FOURIER Note optional name-value pair 'forceReal'.
            p = inputParser;
            p.addRequired('freq');
            p.addRequired('coeff');
            p.addParameter('forceReal', false);
            p.parse(freq, coeff, varargin{:});
            obj.freq = p.Results.freq;
            obj.d = size(obj.freq, 2);
            obj.coeff = p.Results.coeff;
            obj.forceReal = p.Results.forceReal;
        end
        
        function y = eval(obj,x)
            %EVAL Evaluate gaussian series on row-major array of points
            y = exp(2i * pi .* x * obj.freq') * obj.coeff;
            if obj.forceReal
                y = real(y);
            end
        end
        
        function y = evalGrad(obj,x)
            %EVALGRAD Evaluate gradient on row-major array of points
            y = exp(2i * pi .* x * obj.freq') * ...
                (2i * pi .* obj.coeff .* obj.freq);
        end
        
        function y = evalLaplacian(obj,x)
            %EVALGRAD Evaluate Laplacian on row-major array of points
            y = exp(2i * pi .* x * obj.freq') * (-(2 * pi)^2 .*...
                obj.coeff .* sum(obj.freq .* obj.freq, 2));
        end

        function obj = threshold(obj, tol)
            obj.freq = obj.freq(abs(obj.coeff) > tol, :);
            obj.coeff = obj.coeff(abs(obj.coeff) > tol);
        end
        
        function err = relError(obj, v)
            %RELERROR Compute relative L^2 error against a Fourier series
            %NOTE: Error is computed relative to obj
            diff = obj - v;
            err = norm(diff.coeff) / norm(obj.coeff);
        end

        function c = plus(a, b)
            if isa(b, "double")
                newFreq = a.freq;
                newCoeff = a.coeff;
                zeroPos = ismember(newFreq, zeros(1, a.d),...
                    'rows');
                if any(zeroPos)
                    newCoeff(zeroPos) = newCoeff(zeroPos) + b;
                else
                    newFreq = [zeros(1, a.d); newFreq];
                    newCoeff = [b; newCoeff];
                end
                c = fourierSeries(newFreq, newCoeff, ...
                    'forceReal', a.forceReal);
            elseif isa(b, "fourierSeries")
                if isa(a, "fourierSeries")
                    newFreq = union(a.freq, b.freq, 'rows');
                    newCoeff = zeros(size(newFreq, 1), 1);
                    [aPart, aIdx] = ismember(newFreq, a.freq, 'rows');
                    newCoeff(aPart) = newCoeff(aPart) ...
                        + a.coeff(nonzeros(aIdx));
                    [bPart, bIdx] = ismember(newFreq, b.freq, 'rows');
                    newCoeff(bPart) = newCoeff(bPart) ...
                        + b.coeff(nonzeros(bIdx));
                    c = fourierSeries(newFreq, newCoeff, ...
                        'forceReal', a.forceReal & b.forceReal);
                else
                    c = b + a;
                end
            else
                errMsg = ['Cannot add fourierSeries with type ', class(b)];
                ME = MException('fourierSeries:typeException', ...
                    errMsg, str);
                throw(ME);
            end
        end

        function c = times(a, b)
            if isa(a, "double")
                c = fourierSeries(b.freq, a * b.coeff, ...
                    'forceReal', b.forceReal);
            else
                c = fourierSeries(a.freq, b * a.coeff, ...
                    'forceReal', a.forceReal);
            end
        end

        function b = uminus(a)
            b = -1 .* a;
        end

        function c = minus(a, b)
            c = a + -b;
        end

        function b = conj(a)
            b = fourierSeries(-a.freq, conj(a.coeff), 'forceReal', a.forceReal);
        end
    end
end

