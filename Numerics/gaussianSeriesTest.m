addpath('utils');

a = gaussianSeries([4 -6; 1, 2], [-1; 2], [2; 1])

N = 100;
grid = gridAsRowVectors(2, N);

xx = reshape(grid(:, 1), [N, N]);
yy = reshape(grid(:, 2), [N, N]);
zz = reshape(a.eval(grid), [N, N]);
surf(xx, yy, zz)
%%
b = a + 2

zz = reshape(b.eval(grid), [N, N]);
surf(xx, yy, zz)
%%
c = b - a

zz = reshape(c.eval(grid), [N, N]);
surf(xx, yy, zz)
%%
d = a - b

zz = reshape(d.eval(grid), [N, N]);
surf(xx, yy, zz)
%%
e = -a

zz = reshape(e.eval(grid), [N, N]);
surf(xx, yy, zz)
%%
f = 2 .* a

zz = reshape(f.eval(grid), [N, N]);
surf(xx, yy, zz)
%%
g = a + a

zz = reshape(g.eval(grid), [N, N]);
surf(xx, yy, zz)
%%
h = f - g

zz = reshape(h.eval(grid), [N, N]);
surf(xx, yy, zz)