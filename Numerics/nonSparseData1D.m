clf

% This file tests methods for showing numerical solutions are accurate
% without necessarily knowing the exact solution

addpath('Rank1LatticeSparseFourier/algorithms');
addpath('Rank1LatticeSparseFourier/SublinearSparseFourierMATLAB');

% Diffusion coefficient
% a = @(x)1 ./ (2 + sin(20 * pi * sum(x, 2)));
% aTheta = pi / 3;
% a = @(x)1 ./ ((2 + sin(20 * pi * sum(x, 2))) .* (2 + sin(2 * pi * x * round(10 * [cos(aTheta); sin(aTheta)]))));
% a = @(x)2 * ones(size(x, 1), 1);

d = 1;
sVals = [9, 18, 27];
nVals = [1];
gridErrors = zeros(length(sVals), length(nVals));
randErrors = zeros(length(sVals), length(nVals));


epsilon = 1/128;
%     a = @(x)3 ./ (5 + 3 * sin(2 * pi * x ./ epsilon)); % Daubechies Testcase 1
a = @(x)exp((0.6 + 0.2 * cos(2 * pi * x)) ./ (1 + 0.7 * sin(2 * pi * x ./ epsilon))) ./ 10; % Daubechies Testcase 2
% [~, a_min] = fminsearch(a, 0.5 * ones(1, d))
%     A = norm(a_no_mean, 1) / (a_min - 2 * norm(a_no_mean, 1));

% Forcing function
fStart = @(x)exp(-cos(2 * pi * x)); % Multivariate version of Daubechies Testcase 1
fMean = integral(fStart, 0, 1);
f = @(x)fStart(x) - fMean;


xN = 1000;
x = (0:(xN - 1)).' ./ xN;
randN = 2^5;

fName = ['cache/nonsparse1d_N_', num2str(xN), '.mat'];
if exist(fName, 'file')
    load(fName)
else
    
    % tic
    % true = trueSolution(a, f, x);
    % toc
    tic
    trueSol = trueSolutionPar(a, f, x);
    toc

    save(fName, 'trueSol');
end

% subplot(1, 2, 1)
plot(x, trueSol - mean(trueSol), '--', 'LineWidth', 2, 'DisplayName', 'true $u$');
hold on
% subplot(1, 2, 2)
% plot(x, f(x), '--', 'LineWidth', 2, 'DisplayName', 'true $f$');
% hold on
    
for sIdx = 1:length(sVals)
    s = sVals(sIdx)
    % Find Fourier coefficients of a and f


    bandwidth = 1536;

    if ~exist('primeList','var') || length(primeList) < 10000
        primeList = primes(104740); % First 10,000 primes
    end

    % Randomly construct rank-1 lattice for size s frequency set on (-N/2,
    % N/2]^2 cube
%     sigma = 10;
%     threshold = max([sigma * s^2, bandwidth]) + 1;
%     M = min(primeList(primeList > 1000));
%     z = randi(M - 1, 1, d)

    
    plan = SFTPlan(bandwidth, s, 'random', 'primeList', primeList, 'C', 1, 'primeShift', 50, 'randomScale', 1);
    [fCoeff, fFreq] = plan.execute(f(plan.nodes));
    fCoeff = fCoeff.';
    fFreq = fFreq';
    [aCoeff, aFreq] = plan.execute(a(plan.nodes));
    aCoeff = aCoeff.';
    aFreq = aFreq';
%     [fFreq, fCoeff, ~, ~] = phase_enc_fct(z, M, bandwidth, s, 'random', false, false, f, ...
%         'primeList', primeList, 'C', 1, 'alpha', 1, 'beta', 1, 'primeShift', 50, 'randomScale', 1);
%     [aFreq, aCoeff, ~, ~] = phase_enc_fct(z, M, bandwidth, s, 'random', false, false, a, ...
%         'primeList', primeList, 'C', 1, 'alpha', 1, 'beta', 1, 'primeShift', 50, 'randomScale', 1);


%     fFreq = fFreq(abs(fCoeff) > 1e-10, :); fCoeff = fCoeff(abs(fCoeff) > 1e-10);
%     aFreq = aFreq(abs(aCoeff) > 1e-10, :); aCoeff = aCoeff(abs(aCoeff) > 1e-10);
    [~, fRealPos, ~] = intersect(fFreq, -fFreq, 'rows');
    fFreq = fFreq(fRealPos, :);
    fCoeff = fCoeff(fRealPos);
    fOtherRecovered = @(x) real(exp(2i*pi .* x * fFreq') * fCoeff);
    [~, aRealPos, ~] = intersect(aFreq, -aFreq, 'rows');
    aFreq = aFreq(aRealPos, :);
    aCoeff = aCoeff(aRealPos);
    aRecovered = @(x) real(exp(2i*pi .* x * aFreq') * aCoeff);



    for nIdx = 1:length(nVals) % "Stamp" level
        n = nVals(nIdx)
        % Setup system
        % Construct all differences of fFreq and aFreq
        k = stamp(aFreq, fFreq, n);

        % Setup and solve matrix equation
        fCoeffAugment = zeros(size(k, 1), 1);
        [ink, fPos] = ismember(k, fFreq, 'rows'); % We know every element of fFreq is in k since aFreq should include 0 (a can't be mean 0)
        fCoeffAugment(ink) = fCoeff(fPos(ink));
        L = diffusionMatrix(aFreq, aCoeff, k);
        uCoeffTemp = L \ fCoeffAugment;
        uFreq = k; uCoeff = uCoeffTemp;
        recovered = @(x) real(exp(2i*pi .* x * uFreq') * uCoeff);
        recoveredSol = recovered(x);
        error = norm(trueSol - mean(trueSol) - recoveredSol - mean(recoveredSol)) / norm(trueSol - mean(trueSol));
        clear L
%         subplot(1, 2, 1)
        plot(x, recoveredSol, 'DisplayName', ['$s = ', num2str(s), '$, $L^2$ error = ', num2str(error, '%1.4e')]);
        hold on
        
%         % Compute exact Fourier errors in the case where the data is
%         % actually sparse
%         aDif = permute(aFreqTrue, [3, 1, 2]); % Move 2d frequencies into third dimension of tensor
%         kNext = reshape(permute(k, [1, 3, 2]) - aDif, [], 1, d);
%         kNext = unique(squeeze(kNext), 'rows');
%         kNext = setdiff(kNext, zeros(1, d), 'rows');
%         clear k
% %         kNext = stamp(aFreqTrue, fFreqTrue, n + 1);
%         LNext = diffusionMatrix(aFreqTrue, aCoeffTrue, kNext);
%         uCoeffAugment = zeros(size(kNext, 1), 1);
%         [ink, uPos] = ismember(kNext, uFreq, 'rows');
%         uCoeffAugment(ink) = uCoeff(uPos(ink));
%         fApprox = LNext * uCoeffAugment;
%         clear LNext
%         fTrue = zeros(size(kNext, 1), 1);
%         [ink, fPos] = ismember(kNext, fFreqTrue, 'rows'); % We know every element of fFreq is in k since aFreq should include 0 (a can't be mean 0)
%         fTrue(ink) = fCoeffTrue(fPos(ink));
%         fourierErrors(n_idx) = norm(fApprox - fTrue) ./ norm(fTrue);

        % Compute the right hand side from the approximate solution
%         z = sym('z', [1, d]);
%         recoveredSym = symfun(exp(2i * pi .* z * uFreq') * uCoeff, z);
%         aSym = symfun(a(z),z);
%         fRecoveredSym = divergence(a(z) .* gradient(recoveredSym, z), z);
%         fRecovered = matlabFunction(fRecoveredSym);
% 
%         % Full grid
%         fVals = f(x);
%         gridErrors(sIdx, nIdx) = norm(fVals - fRecovered(x)) / norm(fVals);
%         subplot(1, 2, 2);
%         plot(x, fRecovered(x), 'DisplayName', ['$f^{', num2str(s), ',', num2str(n), '}$ error = ', num2str(gridErrors(sIdx, nIdx))]);
%         hold on
% 
%         % Random
%         numPoints = randN;
%         points = rand(numPoints, d);
%         fValsRand = f(points);
%         randErrors(sIdx, nIdx) = norm(fValsRand - fRecovered(points)) / norm(fValsRand);

    end

    % semilogy(nVals, gridErrors)
%     semilogy(nVals, randErrors, '-x', 'DisplayName', ['$s=', num2str(s), '$ random errors'])
%     semilogy(nVals, fourierErrors, '-x', 'DisplayName', ['$d=', num2str(d), '$: Fourier errors'])

end
% figure
% for nIdx = 1:length(nVals)
%     semilogy(sVals, randErrors(:, nIdx).', '-x', 'DisplayName', ['$n=', num2str(nVals(nIdx)), '$ random errors'])
%     hold on
% end
% legend('show', 'Interpreter', 'latex', 'Location', 'southwest')
% xlabel('$s$ (sparsity)', 'Interpreter', 'latex')
% ylabel('Proxy for $\|u - u^{n,s}\|_{H^1}$', 'Interpreter', 'latex')
% hold off
% disp(gridErrors)
% disp(randErrors)

% figure
% plot(x, f(x));
% hold on
% plot(x, fOtherRecovered(x));
% plot(x, fRecovered(x));
% plot(x, a(x));
% hold on
% plot(x, aRecovered(x));

% subplot(1, 2, 1)
legend('show', 'Interpreter', 'latex', 'Location', 'southwest', 'FontSize', 15);
% subplot(1, 2, 2)
% legend('show', 'Interpreter', 'latex');

function k = stamp(aFreq, fFreq, n)
    d = size(aFreq, 2);
    aDif = permute(aFreq, [3, 1, 2]); % Move 2d frequencies into third dimension of tensor
    k = reshape(permute(fFreq, [1, 3, 2]) - aDif, [], 1, d); % Take advantage of implicit array expansion
    for i = 2:n
        k = reshape(k - aDif, [], 1, d);
    end
    k = unique(squeeze(k), 'rows');
    k = setdiff(k, zeros(1, d), 'rows');
end

function L = diffusionMatrix(aFreq, aCoeff, k)
    [in, aPos] = ismember(reshape(shiftdim(reshape(k', [1, size(k')]) - k, 2), [], size(aFreq, 2)), aFreq, 'rows');
    L = reshape(-(2 * pi)^2 .* (k * k'), [], 1);
    L = L .* in;
    L(in) = L(in) .* aCoeff(aPos(in));
    L = reshape(L, [], size(k, 1));
end

function y = trueSolution(a, f, x)
    intermediate = @(t)integral(f, 0, t) ./ a(t);
    intermediateArr = @(t)arrayfun(intermediate, t);
    solution = @(x)integral(intermediateArr, 0, x);
    y = arrayfun(solution, x);
end

function y = trueSolutionPar(a, f, x)
    intermediate = @(t)integral(f, 0, t) ./ a(t);
    intermediateArr = @(t)arrayfun(intermediate, t);
    solution = @(x)integral(intermediateArr, 0, x);
    y = zeros(1, numel(x));
    parfor i = 1:numel(x)
        y(i) = solution(x(i));
    end
    y = reshape(y, size(x));
end