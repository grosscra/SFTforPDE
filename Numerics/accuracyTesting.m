% This file tests methods for showing numerical solutions are accurate
% without necessarily knowing the exact solution

addpath('Rank1LatticeSparseFourier/algorithms');
addpath('Rank1LatticeSparseFourier/SublinearSparseFourierMATLAB');

% Diffusion coefficient
% a = @(x)1 ./ (2 + sin(20 * pi * sum(x, 2)));
% aTheta = pi / 3;
% a = @(x)1 ./ ((2 + sin(20 * pi * sum(x, 2))) .* (2 + sin(2 * pi * x * round(10 * [cos(aTheta); sin(aTheta)]))));
% a = @(x)2 * ones(size(x, 1), 1);

a = @(x)2 + 0.25 .* sin(2 * pi * x * [2; 1]);

% Forcing function
% fTheta = 7 * pi / 6;
% f = @(x)200 * pi^2 * cos(10 * pi * sum(x, 2)) .* sin(8 * pi * x * round(10 * [cos(fTheta); sin(fTheta)]));
f = @(x)cos(2*pi*x*[1;2]);
% f = @(x)cos(2 * pi * x * [5; 3]) - 500 * cos(2 * pi * x * [74; 76]) + 1000 * cos(2 * pi * x * [151; 149]) ;
%.* sin(2 * pi * x * [100; 3]);

%% Plot a and f

N = 2^8;
x = (0:(N - 1)) ./ N;
[xx, yy] = meshgrid(x);

figure

subplot(1, 2, 1)
aVals = reshape(a([xx(:), yy(:)]), size(xx));
mesh(xx, yy, aVals)

subplot(1, 2, 2)
fVals = reshape(f([xx(:), yy(:)]), size(xx));
mesh(xx, yy, fVals)
%%
close all
PLOT = false;

% Find Fourier coefficients of a and f

d = 2;

bandwidth = 500;

if ~exist('primeList','var') || length(primeList) < 10000
	primeList = primes(104740); % First 10,000 primes
end

N = 2^8;
x = (0:(N - 1)) ./ N;
[xx, yy] = meshgrid(x);


s = 10;

% Randomly construct rank-1 lattice for size s frequency set on (-N/2,
% N/2]^2 cube
% s = 10;
sigma = 10;
threshold = max([sigma * s^2, bandwidth]) + 1;
M = min(primeList(primeList > 1000));
z = randi(M - 1, 1, d);

[fFreq, fCoeff, ~, ~] = phase_enc_fct(z, M, bandwidth, s, 'random', false, false, f, ...
    'primeList', primeList, 'C', 1, 'alpha', 1, 'beta', 1, 'primeShift', 50, 'randomScale', 1);
[aFreq, aCoeff, ~, ~] = phase_enc_fct(z, M, bandwidth, s, 'random', false, false, a, ...
    'primeList', primeList, 'C', 1, 'alpha', 1, 'beta', 1, 'primeShift', 50, 'randomScale', 1);


fFreq = fFreq(abs(fCoeff) > 1e-10, :); fCoeff = fCoeff(abs(fCoeff) > 1e-10);
aFreq = aFreq(abs(aCoeff) > 1e-10, :); aCoeff = aCoeff(abs(aCoeff) > 1e-10);

%     fFreq = [-151 -149; 151 149; 74 76; -74 -76; 5 3; -5 -3];
%     fCoeff = [500; 500; -250; -250; 0.5; 0.5];
%     aFreq = [0 0; -1 -2; 1 2]; aCoeff = [2; 0.5i; -0.5i];

% Setup system
% Construct all differences of fFreq and aFreq
n = 1; % "Stamp" level
aDif = permute(aFreq, [3, 1, 2]); % Move 2d frequencies into third dimension of tensor
k = reshape(permute(fFreq, [1, 3, 2]) - aDif, [], 1, d); % Take advantage of implicit array expansion
for i = 2:n
    k = reshape(k - aDif, [], 1, d);
end
k = unique(squeeze(k), 'rows');
k = setdiff(k, zeros(1, d), 'rows'); % Solution is unique up to mean; ignore zero frequency when solving

fCoeffAugment = zeros(size(k, 1), 1);
[ink, fPos] = ismember(k, fFreq, 'rows'); % We know every element of fFreq is in k since aFreq should include 0 (a can't be mean 0)
fCoeffAugment(ink) = fCoeff(fPos(ink));
[in, aPos] = ismember(reshape(shiftdim(reshape(k', [1, size(k')]) - k, 2), [], size(aFreq, 2)), aFreq, 'rows');
convMat = reshape(-(2 * pi)^2 .* (k * k'), [], 1);
convMat = convMat .* in;
convMat(in) = convMat(in) .* aCoeff(aPos(in));
convMat = reshape(convMat, [], size(k, 1));
uCoeffTemp = convMat \ fCoeffAugment;
uFreq = k; uCoeff = uCoeffTemp;
recovered = @(x) exp(2i*pi .* x * uFreq') * uCoeff;


if PLOT
    uSparse = reshape(real(recovered([xx(:), yy(:)])), size(xx)); % Probably don't need, useful for visualizing/debugging
    mesh(xx, yy, uSparse)
end

%% Compute the right hand side from the approximate solution
z = sym('z', [1, d]);
recoveredSym = symfun(exp(2i * pi .* z * uFreq') * uCoeff, z);
aSym = symfun(a(z),z);
fRecoveredSym = divergence(a(z) .* gradient(recoveredSym, z), z);
fRecovered = matlabFunction(fRecoveredSym);
%% Evaluation
% Full grid
fVals = f([xx(:), yy(:)]);
norm(fVals - fRecovered(xx(:), yy(:))) / norm(fVals)
% Random
numPoints = N;
points = rand(numPoints, d);
fValsRand = f(points);
norm(fValsRand - fRecovered(points(:, 1), points(:, 2))) / norm(fValsRand)

if PLOT
    mesh(xx, yy, reshape(f([xx(:), yy(:)]), size(xx)))
    figure
    mesh(xx, yy, reshape(real(fRecovered(xx(:), yy(:))), size(xx)))
end
