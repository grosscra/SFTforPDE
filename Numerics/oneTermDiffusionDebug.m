addpath('../utils');
N = 100; % Maximum bandwidth of data in any dimension
s = 4; % Sparsity of Fourier data
d = 2; % Dimensions of problem
n = 5; % Stamping levels

fourierError = 0;
while(fourierError < 1e-1)
    % Diffusion coefficient
%     aNumTerms = 1;
%     aCoeff = 2 * rand(aNumTerms, 1) - 1;
%     while any(abs(aCoeff) < 1e-1)
%         aCoeff = 2 * rand(aNumTerms, 1) - 1;
%     end
%     aFreq = randi([1, N], aNumTerms, d) - floor(N / 2);
    aFreq = [38, -40];
    aCoeff = 0.4172;
    % Add constant term for ellipticity
    a = fourierSeriesCosine(aFreq, aCoeff) + 4 * ceil(norm(aCoeff));
    
    % Forcing function
    fFreq = [6, -7];
%     fFreq = randi([1, N], 1, d) - floor(N / 2);
    f = fourierSeriesSine(fFreq, 1);
    
    
    diff = sparseADR(f.freq, f.coeff, a.freq, a.coeff, ...
        'sparseMatrix', true);
    diffApprox = sparseADRApproximate(d, s, N + 1, @f.eval, @a.eval, ...
        'aGrad', @a.evalGrad, 'sparseMatrix', false);
    
    
    % Error evaluation grid (100 points in each direction)
    grid = gridAsRowVectors(d, 200);
    
    u = diffApprox.solve(n);
    fApprox = diff.applyToFourierSparse(u, n);
    fVals = f.eval(grid);
    fValsApprox = fApprox.eval(grid);
    error = norm(fVals - fValsApprox) / norm(fVals);
    fourierError = f.relError(fApprox);
    disp(['grid error on object: ', num2str(error)])
    disp(['Fourier error on object: ', num2str(fourierError)])
    
%     [u, fApprox] = solveSparseADR(d, s, n, N + 1, @f.eval, @a.eval, ...
%         'aGrad', @a.evalGrad, 'applyOperator', true, 'sigma', 0.8);
%     fVals = f.eval(grid);
%     fValsApprox = fApprox(grid);
%     error = norm(fVals - fValsApprox) / norm(fVals);
%     disp(['grid error on function: ', num2str(error)])
end