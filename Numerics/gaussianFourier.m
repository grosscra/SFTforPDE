%% 1d

M = 10;
modulation = [10, 30];
C = 2;

N = 2^9;
x = (0 : (N - 1)) ./ N;
omega = -ceil(N/2):floor(N/2) - 1;

reshapeDims = [ones(1, ndims(x)), 2 * M + 1];


y1 = exp(2i .* pi .* modulation(1) .* x) .* ...
    sum(exp(-2 .* (pi .* (mod(x, 1) - ...
    reshape(-M:M, reshapeDims))).^2 ...
    ./ (C .^ 2)), ndims(x) + 1)	./ C;

y2 = exp(2i .* pi .* modulation(2) .* x) .* ...
    sum(exp(-2 .* (pi .* (mod(x, 1) - ...
    reshape(-M:M, reshapeDims))).^2 ...
    ./ (C .^ 2)), ndims(x) + 1)	./ C;

y = (y1 + conj(y1)) ./ 2 + (y2 + conj(y2)) ./ 2 + 10;

y = gaussianFunc(x, modulation(1), C) + gaussianFunc(x, modulation(2), C) + 10;

% y = gaussianSeries(x, modulation', C * ones(length(modulation), 1)) + 10;

yHat = fftshift((1 / N) * fft(y));

clf
subplot(2, 1, 1)
plot(omega, abs(yHat), 'x')
tol = 1e-10;
title(['DFT: $N = ', num2str(N), ', s \approx ', num2str(nnz(abs(yHat) > tol)), '$'], 'Interpreter', 'latex') 
subplot(2, 1, 2)
plot(x, y)
title(['Signal: periodicity $\approx ', num2str(abs(y(1) - y(end))), '$'], 'Interpreter', 'latex')


%% 2d
M = 10;
modulation = [0, 30];
C = 3;

N = 2^10;
x = (0 : (N - 1)) ./ N;
[X, Y] = meshgrid(x);
x = [X(:), Y(:)];
omega = -ceil(N/2):floor(N/2) - 1;

y = gaussianSeries(x, [4, -6; 1, 15], [1; 1]);
y = reshape(y, [N, N]);

yHat = fftshift(fftshift((1 / N^2) * fft2(y), 1), 2);

clf
subplot(2, 1, 1)
mesh(abs(yHat))
tol = 1e-10;
title(['DFT: $N = ', num2str(N), ', s \approx ', num2str(nnz(abs(yHat) > tol)), '$'], 'Interpreter', 'latex') 
subplot(2, 1, 2)
% y = fftshift(fftshift(y, 1), 2);
mesh(X, Y, y)
title(['Signal: periodicity $\approx ', num2str(abs(y(1) - y(end))), '$'], 'Interpreter', 'latex')

%%

d = 2;
addpath('Rank1LatticeSparseFourier/algorithms');
addpath('Rank1LatticeSparseFourier/SublinearSparseFourierMATLAB');
g = @(x)gaussianSeries(x, [4, -6; 1, 15], [1; 2]);

bandwidth = N;

if ~exist('primeList','var') || length(primeList) < 10000
    primeList = primes(104740); % First 10,000 primes
end

s = 50;

sigma = 10;
threshold = max([sigma * s^2, bandwidth]) + 1;
M = min(primeList(primeList > 1000));
z = randi(M - 1, 1, d);

[gFreq, gCoeff, ~, ~] = phase_enc_fct(z, M, bandwidth, s, 'random', false, false, g, ...
    'primeList', primeList, 'C', 1, 'alpha', 1, 'beta', 1, 'primeShift', 50, 'randomScale', 1)

gRecovered = @(x) real(exp(2i * pi .* x * gFreq') * gCoeff);

N = 2^10;
x = (0 : (N - 1)) ./ N;
[X, Y] = meshgrid(x);
x = [X(:), Y(:)];
% omega = -ceil(N/2):floor(N/2) - 1;

y = g(x);
y = reshape(y, [N, N]);
yRecovered = gRecovered(x);
yRecovered = reshape(yRecovered, [N, N]);
% 
% yHat = fftshift(fftshift((1 / N^2) * fft2(y), 1), 2);

clf
subplot(2, 2, 1)
mesh(X, Y, y)
title('$g$', 'Interpreter', 'latex');
% tol = 1e-10;
% title(['DFT: $N = ', num2str(N), ', s \approx ', num2str(nnz(abs(yHat) > tol)), '$'], 'Interpreter', 'latex') 
subplot(2, 2, 2)
mesh(X, Y, yRecovered)
title(['$g^s$, $s = ', num2str(s), '$'], 'Interpreter', 'latex');
subplot(2, 2, 3)
diff = abs(y - yRecovered);
mesh(X, Y, diff);
title(['relative $L^2$ error $= ', num2str(sqrt(sum(diff.^2, 'all') / sum(y.^2, 'all'))), '$, $\|g - g^s\|_{L^\infty} = ', num2str(max(diff, [], 'all')), '$'], 'Interpreter', 'latex');
% y = fftshift(fftshift(y, 1), 2)
% title(['Signal: periodicity $\approx ', num2str(abs(y(1) - y(end))), '$'], 'Interpreter', 'latex')


%%

d = 2;
addpath('Rank1LatticeSparseFourier/algorithms');
addpath('Rank1LatticeSparseFourier/SublinearSparseFourierMATLAB');
addpath('utils');
g = gaussian([4, -6; 1, 15], [-1; 2]);

bandwidth = N;

if ~exist('primeList','var') || length(primeList) < 10000
    primeList = primes(104740); % First 10,000 primes
end

s = 50;

sigma = 10;
threshold = max([sigma * s^2, bandwidth]) + 1;
M = min(primeList(primeList > 1000));
z = randi(M - 1, 1, d);

[gFreq, gCoeff, ~, ~] = phase_enc_fct(z, M, bandwidth, s, 'random', false, false, @g.eval, ...
    'primeList', primeList, 'C', 1, 'alpha', 1, 'beta', 1, 'primeShift', 50, 'randomScale', 1)

gRecovered = @(x) real(exp(2i * pi .* x * gFreq') * gCoeff);

N = 2^10;
x = (0 : (N - 1)) ./ N;
[X, Y] = meshgrid(x);
x = [X(:), Y(:)];
% omega = -ceil(N/2):floor(N/2) - 1;

y = g.eval(x);
y = reshape(y, [N, N]);
yRecovered = gRecovered(x);
yRecovered = reshape(yRecovered, [N, N]);
% 
% yHat = fftshift(fftshift((1 / N^2) * fft2(y), 1), 2);

clf
subplot(2, 2, 1)
mesh(X, Y, y)
title('$g$', 'Interpreter', 'latex');
% tol = 1e-10;
% title(['DFT: $N = ', num2str(N), ', s \approx ', num2str(nnz(abs(yHat) > tol)), '$'], 'Interpreter', 'latex') 
subplot(2, 2, 2)
mesh(X, Y, yRecovered)
title(['$g^s$, $s = ', num2str(s), '$'], 'Interpreter', 'latex');
subplot(2, 2, 3)
diff = abs(y - yRecovered);
mesh(X, Y, diff);
title(['relative $L^2$ error $= ', num2str(sqrt(sum(diff.^2, 'all') / sum(y.^2, 'all'))), '$, $\|g - g^s\|_{L^\infty} = ', num2str(max(diff, [], 'all')), '$'], 'Interpreter', 'latex');
subplot(2, 2, 4)
imagesc(fftshift(fftshift(abs(fft2(yRecovered)), 1), 2))
% y = fftshift(fftshift(y, 1), 2)
% title(['Signal: periodicity $\approx ', num2str(abs(y(1) - y(end))), '$'], 'Interpreter', 'latex')

%%

M = 10;
gdx = @(x, y)(2i * pi * 2 * exp(2i * pi * 2 * x) * sum(exp(-(2 * pi)^2 * (x - (-M:M)).^2 ./ (2 * 7^2))) ...
    + exp(2i * pi * 2 * x) * sum(-(2 * pi / 7)^2 * (x - (-M:M)) .* exp(-(2 * pi)^2 * (x - (-M:M)).^2 ./ (2 * 7^2)))) ...
    * exp(2i * pi * -3 * y) * sum(exp(- (2 * pi)^2 * (y - (-M:M)) .^2 ./ (2 * 7^2)));

g = gaussianSeries([2, -3], 7);
gdx(0.5, 0.4)
gd = g.evalGrad([0.5, 0.4]);
gd(1)

%%

function y = gaussianFunc(x, modulation, C)
    M = 10;
    d = size(x, 2); % x is rows of points
    reshapeDims = [ones(1, d), 2 * M + 1];

%     y = exp(2i .* pi .* modulation(1) .* x) .* sum(exp(-2 .* (pi .* (mod(x, 1) - reshape(-M:M, reshapeDims))).^2 ./ (C .^ 2)), ndims(x) + 1) ./ C;
    modulator = exp(2i .* pi .* modulation .* x);
    y = real(prod(modulator .* sum(exp(-2 .* (pi .* (mod(x, 1) - reshape(-M:M, reshapeDims))).^2 ./ (C .^ 2)), d + 1), d));

end

function y = gaussianGrad(x, modulation, C)
    M = 10;
    d = size(x, 2); % x is rows of points
    reshapeDims = [ones(1, d), 2 * M + 1];
    
    y = (2i * pi)^3 * modulation .* sum(x - reshape(-M:M, reshapeDims), d + 1) .* g(x) ./ C^2;
%     y = real(prod(modulator .* sum(exp(-2 .* (pi .* (mod(x, 1) - reshape(-M:M, reshapeDims))).^2 ./ (C .^ 2)), d + 1), d));
end

% function y = gaussianSeries(x, modulation, C)
%     y = zeros(size(x, 1), 1);
%     for i = 1:size(modulation, 1)
%         y = y + gaussianFunc(x, modulation(i, :), C(i));
%     end
% end
% 
% function y = gaussianSeriesGrad(x, modulation, C)
%     y = zeros(size(x, 1), size(modulation, 2));
%     for i = 1:size(modulation, 1)
%         y = y + gaussianGrad(x, modulation(i, :), C(i));
%     end
% end
