%% n growth
clf
lines = ["-", "--", ":", "-."];
points = ["o", "+", "*", "x", "."];
s = 5;
for d = 1:6
    fprintf('----d=%d----\n', d)
    aFreq = randi(100, s, d) - 50;
    aFreq = [zeros(1, d); aFreq; -aFreq];
    fFreq = randi(100, s, d) -50;
    fFreq = [fFreq; -fFreq];

    nVals = 1:12;
    asymptoticThreshold = floor(length(nVals) / 2);
    stampSizes = zeros(1, length(nVals));
    stampLoopSizes = zeros(1, length(nVals));
    for nIdx = 1:length(nVals)
        disp(nVals(nIdx))
%         tic
%         stampSizes(nIdx) = size(stamp(aFreq, fFreq, nVals(nIdx)), 1);
%         fprintf('stamp time = %f\n', toc)
        tic
%         stampLoop(aFreq, fFreq, nVals(nIdx))
        stampLoopSizes(nIdx) = size(stampLoop(aFreq, fFreq, nVals(nIdx)), 1);
        fprintf('stamp loop time = %f\n', toc)
    end
    line = lines(mod(d, length(lines)) + 1);
    point = points(mod(d, length(points)) + 1);
    linespec = strcat(line, point);
%     semilogy(nVals, stampSizes, linespec, 'DisplayName', ['d=', num2str(d)])
%     hold on

    % least squares for polynomial coefficients
    lsnVals = log(nVals(asymptoticThreshold:end)');
    lsStamp = log(stampLoopSizes(asymptoticThreshold:end)');
    ls = [lsnVals, ones(size(lsnVals))] \ lsStamp;
    exponent = ls(1);
    coeff = exp(ls(2));
    
    semilogy(nVals, stampLoopSizes, linespec, 'DisplayName', ['$d=', num2str(d), '$ (loop). $\approx', num2str(coeff),'n^{', num2str(exponent) '}$'])
    hold on
end
semilogy(nVals, nVals.^(2 * s + 1), 'DisplayName', '$n^s$')
semilogy(nVals, (2 * s + 1).^(nVals + 1), 'DisplayName', '$s^n$')
upperBoundSizes = zeros(1, length(nVals));
for nIdx = 1:length(upperBoundSizes)
    upperBoundSizes(nIdx) = stampSizeUpperBound(s, nVals(nIdx));
end
semilogy(nVals, upperBoundSizes, 'DisplayName', 'Combinatorial upper bound')
legend('show', 'Interpreter', 'latex', 'Location', 'southeast')
xlabel('$n$ (stamping level)', 'Interpreter', 'latex')
ylabel(['$|\mathcal{S}^n|$ (sparsity fixed at ', num2str(s), ')'], 'Interpreter', 'latex')
%% s growth
clf
lines = ["-", "--", ":", "-."];
points = ["o", "+", "*", "x", "."];
n = 6;
for d = 1:4
    fprintf('----d=%d----\n', d)

    sVals = 2:10;
    asymptoticThreshold = floor(length(sVals) / 2);
    stampSizes = zeros(1, length(sVals));
    stampLoopSizes = zeros(1, length(sVals));
    for sIdx = 1:length(sVals)
        aFreq = randi(100, sVals(sIdx), d);
        aFreq = [zeros(1, d); aFreq; -aFreq];
        fFreq = randi(100, sVals(sIdx), d);
        fFreq = [fFreq; -fFreq];
        disp(sVals(sIdx))
%         tic
%         stampSizes(sIdx) = size(stamp(aFreq, fFreq, n), 1);
%         fprintf('stamp time = %f\n', toc)
        tic
        stampLoopSizes(sIdx) = size(stampLoop(aFreq, fFreq, n), 1);
        fprintf('stamp loop time = %f\n', toc)
    end
    line = lines(mod(d, length(lines)) + 1);
    point = points(mod(d, length(points)) + 1);
    linespec = strcat(line, point);
%     semilogy(sVals, stampSizes, linespec, 'DisplayName', ['d=', num2str(d)])
%     hold on

    % least squares for polynomial coefficients
    lssVals = log(sVals(asymptoticThreshold:end)');
    lsStamp = log(stampLoopSizes(asymptoticThreshold:end)');
    ls = [lssVals, ones(size(lssVals))] \ lsStamp;
    exponent = ls(1);
    coeff = exp(ls(2));
    
    semilogy(sVals, stampLoopSizes, linespec, 'DisplayName', ['$d=', num2str(d), '$ (loop). $\approx', num2str(coeff),'s^{', num2str(exponent) '}$'])
    hold on
end
semilogy(sVals, n.^sVals, 'DisplayName', '$n^s$')
semilogy(sVals, sVals.^n, 'DisplayName', '$s^n$')
upperBoundSizes = zeros(1, length(sVals));
for sIdx = 1:length(upperBoundSizes)
    upperBoundSizes(sIdx) = stampSizeUpperBound(sVals(sIdx), n);
end
semilogy(sVals, upperBoundSizes, 'DisplayName', 'Combinatorial upper bound')
legend('show', 'Interpreter', 'latex', 'Location', 'southeast')
xlabel('$s$ (sparsity)', 'Interpreter', 'latex')
ylabel(['$|\mathcal{S}^', num2str(n), '|$'], 'Interpreter', 'latex')

%%
function k = stamp(aFreq, fFreq, n)
    d = size(aFreq, 2);
    aDif = permute(aFreq, [3, 1, 2]); % Move 2d frequencies into third dimension of tensor
    k = reshape(permute(fFreq, [1, 3, 2]) - aDif, [], 1, d); % Take advantage of implicit array expansion
    for i = 2:n
        k = reshape(k - aDif, [], 1, d);
    end
    k = unique(squeeze(k), 'rows');
    k = setdiff(k, zeros(1, d), 'rows');
end

function k = stampLoop(aFreq, fFreq, n)
    if n == 0
        k = fFreq;
    else
        kPrev = stampLoop(aFreq, fFreq, n - 1);
        numPrev = size(kPrev, 1);
        numA = size(aFreq, 1);
        k = zeros(numA * numPrev, size(aFreq, 2));
        for i = 1:numA
            k((1:numPrev) + (i - 1) * numPrev, :) = -aFreq(i, :) + kPrev;
        end
        k = unique(squeeze(k), 'rows');
        k = setdiff(k, zeros(1, size(aFreq, 2)), 'rows');
    end
end

function size = newStampUpperBound(s, n)
    size = 0;
    for i = 1:min(n, s)
        size = size + 2^i * nchoosek(s, s - i) * nchoosek(n - 1, i - 1);
    end
    size = size * (2 * s + 1);
end

function size = stampSizeUpperBound(s, n)
    size = 0;
    for i = 0:n
        size = size + newStampUpperBound(s, i);
    end
end