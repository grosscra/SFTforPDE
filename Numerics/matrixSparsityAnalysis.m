addpath('utils');

close all

N = 1000;
dVals = 2 .^ (2);
nVals = 4;

% Keep diffusion coefficient Fourier coefficients constant
aNumTerms = 20;
aCoeff = 2 * rand(aNumTerms, 1) - 1;
while any(abs(aCoeff) < 1e-1)
    aCoeff = 2 * rand(aNumTerms, 1) - 1;
end


for d_idx = 1:length(dVals)
    d = dVals(d_idx)

    % Diffusion coefficient
    aFreq = randi([1, N], aNumTerms, d) - floor(N / 2);
    % Add constant term for ellipticity
    a = fourierSeriesCosine(aFreq, aCoeff) + 4 * ceil(norm(aCoeff));

    % Forcing function
    fFreq = randi([1, N], 1, d) - floor(N / 2);
    f = fourierSeriesSine(fFreq, 1);
    
    diff = sparseADR(f.freq, f.coeff, a.freq, a.coeff, ...
        'sparseMatrix', true);
    for n = nVals
        L = diff.assembleOperator(diff.stamp(n));
        for permAlg = {@colperm, @symrcm, @colamd, @dissect}
            sparseLUAnalysis(L, permAlg{1});
        end
    end
end

function bw = bandwidth(L)
% Compute the bandwidth of a sparse matrix
        [i, j] = find(L);
        bw = max(i - j) + 1;
end

function sparseLUAnalysis(L, permAlg)
% Plot sparsity of matrix, LU decomposition and permutation of matrix
    
    p = permAlg(L);
    Lperm = L(p, p);

    figure
    subplot(2,2,1)
    spy(L)
    title('L')
    subplot(2,2,2)
    spy(lu(L))
    title('LU Decomposition of L')
    subplot(2,2,3)
    spy(Lperm)
    title([func2str(permAlg), '(L)'])
    subplot(2,2,4)
    spy(lu(Lperm))
    title(['LU Decomposition of ', func2str(permAlg), '(L)'])

    bandwidthL = bandwidth(L);
    bandwidthLperm = bandwidth(Lperm);

    fillInL = nnz(lu(L)) / nnz(L);
    fillInLperm = nnz(lu(Lperm)) / nnz(Lperm);

    fprintf('=== TESTING %s ===\n\n', func2str(permAlg))

    fprintf('bandwidth(L):\n\t%d\n', bandwidthL);
    fprintf(['bandwidth(', func2str(permAlg), '(L)):\n\t%d\n'], bandwidthLperm);
    fprintf('bandwidth ratio:\n\t%f\n\n', bandwidthLperm / bandwidthL);
    
    fprintf('fill-in(L):\n\t%f\n', fillInL);
    fprintf(['fill-in(', func2str(permAlg), '(L)):\n\t%d\n'], fillInLperm);
    fprintf('fill-in ratio:\n\t%f\n\n', fillInLperm / fillInL);

end