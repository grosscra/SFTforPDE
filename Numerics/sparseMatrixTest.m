s = 10;
d = 10;
n = 3;

p = gcp('nocreate');
if isempty(p)
    parpool(8);
end
disp(['====== s = ' num2str(s) ', d = ' num2str(d) ', n = ' num2str(n) ' ======'])

aFreq = randi(1000, s, d) - 500;
aFreq = [zeros(1, d); aFreq; -aFreq];
imagLocations = randi(2, s, 1) - 1;
realLocations = 1 - imagLocations;
aCoeff = 1i .* imagLocations .* rand(s, 1) + realLocations .* rand(s, 1);
aCoeff = [rand() + 1; aCoeff; conj(aCoeff)];
fFreq = randi(1000, s, d) - 500;
fFreq = [fFreq; -fFreq];


% tic
% k = stamp(aFreq, fFreq, n);
% disp(['stamp time = ', num2str(toc)])
tic
kLoop = stampLoop(aFreq, fFreq, n);
disp(['stamp loop time = ', num2str(toc)])
% tic
% L = diffusionMatrix(aFreq, aCoeff, k);
% disp(['dense matrix time = ', num2str(toc)])
keys = num2cell(num2str(kLoop, '%012d'), 2);
map = containers.Map(keys, 1:length(keys));
tic
Lsparse = sparseDiffusionMatrixOld(aFreq, aCoeff, kLoop, map);
disp(['sparse matrix time = ', num2str(toc)])
% diff = L - Lsparse;
% disp(['Frobenius error = ', num2str(norm(diff))])
% disp(['Infinty error = ', num2str(max(abs(diff), [], 'all'))])

% clf
% subplot(1, 3, 1)
% spy(L)
% subplot(1, 3, 2)
% spy(Lsparse)
% subplot(1, 3, 3)
% spy(abs(L - Lsparse) > 1e-5)

function k = stamp(aFreq, fFreq, n)
    d = size(aFreq, 2);
    aDif = permute(aFreq, [3, 1, 2]); % Move 2d frequencies into third dimension of tensor
    k = reshape(permute(fFreq, [1, 3, 2]) - aDif, [], 1, d); % Take advantage of implicit array expansion
    for i = 2:n
        k = reshape(k - aDif, [], 1, d);
    end
    k = unique(squeeze(k), 'rows');
%     k = setdiff(k, zeros(1, d), 'rows');
end

function k = stampLoop(aFreq, fFreq, n)
    d = size(aFreq, 2);
    aDif = permute(aFreq, [3, 1, 2]); % Move 2d frequencies into third dimension of tensor
    tic
    k = reshape(permute(fFreq, [1, 3, 2]) - aDif, [], 1, d); % Take advantage of implicit array expansion
    toc
    for i = 2:n
        reshapedDif = reshape(k - aDif, [], d);
        reshapedk = squeeze(k);
        lia = ismember(reshapedDif, reshapedk, 'rows');
        reshapedk = [reshapedk; reshapedDif(~lia, :)];
        k = reshape(reshapedk, [], 1, d);
    end
    k = unique(squeeze(k), 'rows');
%     k = setdiff(k, zeros(1, d), 'rows');
end

function L = diffusionMatrix(aFreq, aCoeff, k)
    [in, aPos] = ismember(reshape(shiftdim(reshape(k', [1, size(k')]) - k, 2), [], size(aFreq, 2)), aFreq, 'rows');
    L = reshape(-(2 * pi)^2 .* (k * k'), [], 1);
%     L = -(2 * pi)^2 .* ones(length(in), 1)
    L = L .* in;
    L(in) = L(in) .* aCoeff(aPos(in));
    L = reshape(L, [], size(k, 1));
end

function L = sparseDiffusionMatrixSlow(aFreq, aCoeff, k, map)
    val = [];
    row = [];
    col = [];
    parfor jIdx = 1:size(k, 1)
        j = k(jIdx, :);
        freqCell = num2cell(num2str(j - aFreq, '%012d'), 2);
        for aIdx = 1:length(freqCell)
            try
                columnVal = map(freqCell{aIdx});
                val = [val; -(2 * pi)^2 .* (j - aFreq(aIdx, :)) * j' .* aCoeff(aIdx)];
                row = [row; jIdx];
                col = [col; columnVal];
            catch ME
                if(~strcmp(ME.identifier, "MATLAB:Containers:Map:NoKey"))
                    rethrow(ME)
                end
            end
        end
%         aPos = isKey(map, freqCell);
%         colFreq = j - aFreq(aPos, :);
% %         [~, ink, aPos] = intersect(j - k, aFreq, 'rows', 'stable');
%         val = [val; -(2 * pi)^2 .* colFreq * j' .* aCoeff(aPos)];
% %         -(2 * pi)^2 .* k(ink, :) * j' .* aCoeff(aPos)
%         row = [row; jIdx * ones(nnz(aPos), 1)];
% %         jIdx * ones(length(ink), 1)
%         col = [col; cell2mat(values(map, freqCell(aPos)))];
    end
    L = sparse(row, col, val, size(k, 1), size(k, 1));
end

function L = sparseDiffusionMatrix(aFreq, aCoeff, k, map)
    val = [];
    row = [];
    col = [];
    parfor jIdx = 1:size(k, 1)
        j = k(jIdx, :);
        freqCell = num2cell(num2str(j - aFreq, '%012d'), 2);
        aPos = isKey(map, freqCell);
        colFreq = j - aFreq(aPos, :);
%         [~, ink, aPos] = intersect(j - k, aFreq, 'rows', 'stable');
        val = [val; -(2 * pi)^2 .* colFreq * j' .* aCoeff(aPos)];
%         -(2 * pi)^2 .* k(ink, :) * j' .* aCoeff(aPos)
        row = [row; jIdx * ones(nnz(aPos), 1)];
%         jIdx * ones(length(ink), 1)
        col = [col; cell2mat(values(map, freqCell(aPos)))];
    end
    L = sparse(row, col, val, size(k, 1), size(k, 1));
end

function L = sparseDiffusionMatrixNoHashMap(aFreq, aCoeff, k)
    val = [];
    row = [];
    col = [];
    parfor jIdx = 1:size(k, 1)
        j = k(jIdx, :);
        [~, ink, aPos] = intersect(j - k, aFreq, 'rows', 'stable');
        val = [val; -(2 * pi)^2 .* k(ink, :) * j' .* aCoeff(aPos)];
%         -(2 * pi)^2 .* k(ink, :) * j' .* aCoeff(aPos)
        row = [row; jIdx * ones(length(ink), 1)];
%         jIdx * ones(length(ink), 1)
        col = [col; ink];
    end
    L = sparse(row, col, val, size(k, 1), size(k, 1));
end