
addpath('../utils');
N = 100; % Maximum bandwidth of data in any dimension
s = 10; % Sparsity of Fourier data
dVals = 2 .^(1:4); % Dimensions of problem
nVals = 1:5; % Stamping levels

gridErrors = zeros(length(dVals), length(nVals));
fourierErrors = zeros(length(dVals), length(nVals));

% Keep diffusion coefficient Fourier coefficients constant
aNumTerms = 1;
aCoeff = 2 * rand(aNumTerms, 1) - 1;
while any(abs(aCoeff) < 1e-1)
    aCoeff = 2 * rand(aNumTerms, 1) - 1;
end


for d_idx = 1:length(dVals)
    d = dVals(d_idx)

    % Diffusion coefficient
    aFreq = randi([1, N], aNumTerms, d) - floor(N / 2);
    aRate = (1.1 ^ d) * ones(aNumTerms, 1); % TODO: Come up with better sparsity scheme
    % Add constant term for ellipticity
    a = gaussianSeries(aFreq, aCoeff, aRate) + 4 * ceil(norm(aCoeff));

    % Forcing function
    fFreq = randi([1, N], 1, d) - floor(N / 2);
    f = fourierSeriesSine(fFreq, 1);
    
    diffApprox = sparseADRApproximate(d, s, N + 1, @f.eval, @a.eval, ...
        'aGrad', @a.evalGrad, 'sparseMatrix', true);

    if d < 4
        % Uniform error evaluation grid (200 points in each dimension)
        grid = gridAsRowVectors(d, 200);
    else
        % Random grid over 200 points total
        grid = rand(200, d);
    end

    fVals = f.eval(grid);
    
    for n_idx = 1:length(nVals)
        n = nVals(n_idx)
        
        u = diffApprox.solve(n);
        fApprox = diffApprox.apply(u);
        fValsApprox = fApprox(grid);
        gridErrors(d_idx, n_idx) = norm(fVals - fValsApprox) / norm(fVals);
    end
    semilogy(nVals, gridErrors(d_idx, :), '-x', 'DisplayName', ...
        ['$d=', num2str(d), '$ on grid']);
    hold on
end

legend('show', 'Interpreter', 'latex', 'Location', 'southwest');
xlabel('$n$ (stamping level)', 'Interpreter', 'latex')
ylabel('Proxy for $\frac{\|u - u^{n,s}\|_{H^1}}{\|u\|_{H^1}}$',...
    'Interpreter', 'latex')
hold off

savefig("../results/oneGaussianTermDiffusionStamp.fig")
exportgraphics(gcf,...
    '../../Paper/Figures/oneGaussianTermDiffusionStamp.png', ...
    'Resolution',300)