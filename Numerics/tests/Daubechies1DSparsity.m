addpath('../utils')

close all

% Diffusion coefficient (Daubechies et al., Testcase 2)
epsilon = 1/128;
a = @(x)(1/10) .* exp( ...
            (0.6 + 0.2 * cos(2 * pi * x)) ...
            ./ (1 + 0.7 * sin(2 * pi * x ./ epsilon)) ...
        );
aGrad = @(x)-(2 * pi) .* a(x) .*...
            ( ...
                0.2 * sin(2 * pi * x) .* ...
                    (1 + 0.7 * sin(2 * pi * x ./ epsilon)) + ...
                (0.7 / epsilon) * cos(2 * pi * x ./ epsilon) .* ...
                    (0.6 + 0.2 * cos(2 * pi * x)) ...
            ) ...
            ./ ((1 + 0.7 * sin(2 * pi * x ./ epsilon)) .^ 2);

% Forcing function (Daubechies et al., Testcase 2)
fStart = @(x)exp(-cos(2 * pi * x));
fMean = integral(fStart, 0, 1);
f = @(x)fStart(x) - fMean; % Forcing function must be mean-zero

% Parameters
NVals = [1536]; % Bandwidth
sVals = [4, 8, 12]; % Sparsity of data
d = 1;
nVals = 1;

% Compute "exact" solution by integrating

numGridPoints = 10000;
grid = (0:(numGridPoints - 1)).' ./ numGridPoints;

fName = ['../cache/nonsparse1d_N_', num2str(numGridPoints), '.mat'];
if exist(fName, 'file')
    load(fName)
else
    % tic
    % true = trueUnivariateSolution(a, f, grid);
    % toc
    tic
    trueSol = trueUnivariateSolutionParallel(a, f, grid);
    toc

    save(fName, 'trueSol');
end

trueSol = trueSol - mean(trueSol);
figure
plot(grid, trueSol, '--', 'LineWidth', 2, 'DisplayName', '$u$')
hold on

trueDeriv = diff(trueSol) * numGridPoints;
figure
plot(grid(1:end-1), trueDeriv, '--', 'LineWidth', 2, 'DisplayName', "$du/dx$")
hold on

% Precompute right-hand-side on grid

fVals = f(grid);

% Result arrays
gridErrors = zeros(length(sVals), length(NVals));
gridErrorsDeriv = zeros(length(sVals), length(NVals));
gridErrorsProxy = zeros(length(sVals), length(NVals));

n = 1;
for NIdx = 1:length(NVals)
    N = NVals(nIdx)
    for sIdx = 1:length(sVals)
        s = sVals(sIdx)
    
        diffApprox = sparseADRApproximate(1, s, N, f, a, 'aGrad', aGrad, ...
            'sparseMatrix', false);
    %     disp(length(diffApprox.aFreq));
    
        u = diffApprox.solve(n);
        fApprox = diffApprox.apply(u);
    
        recoveredSol = u.eval(grid);
        recoveredDeriv = diff(recoveredSol) * numGridPoints;

        gridErrors(sIdx, NIdx) = norm(recoveredSol - trueSol) ...
                                    / norm(trueSol);

        gridErrorsDeriv(sIdx, NIdx) = norm(recoveredDeriv - trueDeriv) ...
                                    / norm(trueDeriv);
        fValsApprox = fApprox(grid);
        gridErrorsProxy(sIdx, NIdx) = norm(fValsApprox - fVals) ...
                                        / norm(fVals);
        if NIdx == 1
            figure(1)
            plot(grid, recoveredSol, 'DisplayName', sprintf('$u^{%d, 1}$', s))
            hold on
            figure(2)
            plot(grid(1:end-1), recoveredDeriv, 'DisplayName', sprintf("$du^{%d, 1}/dx$", s))
            hold on
        end
    end
end
figure(1)
legend('show', 'Interpreter', 'latex', 'Location', 'southwest');
xlabel('$x$', 'Interpreter', 'latex')
hold off

figure(2)
xlim([4.25, 4.36] ./ (2 * pi))
ylim("auto")
legend('show', 'Interpreter', 'latex', 'Location', 'southwest');
xlabel('$x$', 'Interpreter', 'latex')
hold off


figure(3)
for NIdx = 1:length(NVals)
    semilogy(sVals, gridErrors(:, NIdx), '-x', 'DisplayName', ...
        sprintf('$L^2$'));
    hold on
    semilogy(sVals, gridErrorsDeriv(:, NIdx), '-x', 'DisplayName', ...
        sprintf('$H^1$'));
    hold on
    semilogy(sVals, gridErrorsProxy(:, NIdx), '-o', 'DisplayName', ...
        sprintf('Proxy'));
    hold on
end

legend('show', 'Interpreter', 'latex', 'Location', 'southwest');
xlabel('$s$ (sparsity)', 'Interpreter', 'latex')
ylabel('Relative error', 'Interpreter', 'latex')
hold off


% savefig("../results/Daubechies1D.fig")
set(figure(1),'renderer','painters')
set(figure(2),'renderer','painters')
set(figure(3),'renderer','painters')
exportgraphics(figure(1),'../../Paper/Figures/Daubechies1DSolution.pdf','Resolution',300)
exportgraphics(figure(2),'../../Paper/Figures/Daubechies1DDerivative.pdf','Resolution',300)
exportgraphics(figure(3),'../../Paper/Figures/Daubechies1DErrors.pdf','Resolution',300)