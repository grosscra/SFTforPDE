addpath('../utils');

rng(74);

N = 100; % Maximum bandwidth of data in any dimension
s = 55; % Sparsity of Fourier data
d = 3; % Dimensions of problem
nVals = 1:3; % Stamping levels
computeGridError = true; % Compute errors on grid

if computeGridError
    gridErrors = zeros(1, length(nVals));
end
fourierErrors = zeros(1, length(nVals));

% Diffusion coefficient
aNumTerms = 5;
aSinFreq = randi([1, N], floor(aNumTerms / 2), d) - floor(N / 2);
aSinCoeff = 2 * rand(floor(aNumTerms / 2), 1) - 1;
while any(abs(aSinCoeff) < 1e-1)
    aSinCoeff = 2 * rand(floor(aNumTerms / 2), 1) - 1;
end
aCosFreq = randi([1, N], aNumTerms - floor(aNumTerms / 2), d) ...
    - floor(N / 2);
aCosCoeff = 2 * rand(aNumTerms - floor(aNumTerms / 2), 1) - 1;
while any(abs(aCosCoeff) < 1e-1)
    aCosCoeff = 2 * rand(aNumTerms - floor(aNumTerms / 2), 1) - 1;
end
% Add constant term for ellipticity
shift = ceil(4 * norm([aSinCoeff; aCosCoeff]));
a = fourierSeriesSine(aSinFreq, aSinCoeff) ...
    + fourierSeriesCosine(aCosFreq, aCosCoeff) + shift;

% Forcing function
fNumTerms = 5;
fSinFreq = randi([1, floor(N/2)], floor(fNumTerms / 2), d) - floor(N / 4);
fSinCoeff = 2 * rand(floor(fNumTerms / 2), 1) - 1;
while any(norm(fSinCoeff) < 1e-1)
    fSinCoeff = 2 * rand(floor(fNumTerms / 2), 1) - 1;
end
fCosFreq = randi([1, floor(N/2)], fNumTerms - floor(fNumTerms / 2), d) ...
    - floor(N / 4);
fCosCoeff = 2 * rand(fNumTerms - floor(fNumTerms / 2), 1) - 1;
while any(norm(fCosCoeff) < 1e-1)
    fCosCoeff = 2 * rand(fNumTerms - floor(fNumTerms / 2), 1) - 1;
end
shift = ceil(4 * norm([fSinCoeff; fCosCoeff]));
f = fourierSeriesSine(fSinFreq, fSinCoeff) ...
    + fourierSeriesCosine(fCosFreq, fCosCoeff) + shift;

% Advection term
bCell = cell(1, d);
bHandleCell = cell(1, d);
bFreq = cell(1, d);
bCoeff = cell(1, d);
bNumTerms = 10;
for i = 1:d
    bSinFreq = randi([1, N], floor(bNumTerms / 2), d) - floor(N / 2);
    bSinCoeff = rand(floor(bNumTerms / 2), 1);
    bCosFreq = randi([1, N], bNumTerms - floor(bNumTerms / 2), d)...
        - floor(N / 2);
    bCosCoeff = rand(bNumTerms - floor(bNumTerms / 2), 1);
    bFourier = fourierSeriesSine(bSinFreq, bSinCoeff) ...
        + fourierSeriesCosine(bCosFreq, bSinFreq);
    bCell{i} = bFourier;
    bHandleCell{i} = @bFourier.eval;
    bFreq{i} = bFourier.freq;
    bCoeff{i} = bFourier.coeff;
end

% Reaction term
cNumTerms = 10;
cSinFreq = randi([1, N], floor(cNumTerms / 2), d) - floor(N / 2);
cSinCoeff = rand(floor(cNumTerms / 2), 1);
while any(norm(cSinCoeff) < 1e-1)
    cSinCoeff = 2 * rand(floor(cNumTerms / 2), 1) - 1;
end
cCosFreq = randi([1, 100], cNumTerms - floor(cNumTerms / 2), d) - 50;
cCosCoeff = rand(cNumTerms - floor(cNumTerms / 2), 1);
while any(norm(cCosCoeff) < 1e-1)
    cCosCoeff = 2 * rand(cNumTerms - floor(cNumTerms / 2), 1) - 1;
end
shift = ceil(4 * norm([cSinCoeff; cCosCoeff]));
c = fourierSeriesSine(cSinFreq, cSinCoeff) ...
    + fourierSeriesCosine(cCosFreq, cCosCoeff) + shift;

% Set up ADR equations
diff = sparseADR(f.freq, f.coeff, a.freq, a.coeff, ...
    'bFreq', bFreq, 'bCoeff', bCoeff, ...
    'cFreq', c.freq, 'cCoeff', c.coeff, ...
    'sparseMatrix', true);
diffApprox = sparseADRApproximate(d, s, N + 1, @f.eval, @a.eval, ...
    'b', bHandleCell, 'c', @c.eval, ...
    'aGrad', @a.evalGrad, 'sparseMatrix', true);

% Solve for various stamping levels

if computeGridError
    % Random grid over 1000 points total
    grid = rand(1000, d);
    fVals = f.eval(grid);
end
for nIdx = 1:length(nVals)
    n = nIdx
    u = diffApprox.solve(n);
    fApprox = diff.applyToFourierSparse(u, n);
    if computeGridError
        fApproxHandle = diffApprox.apply(u);
        fValsApprox = fApproxHandle(grid);
        gridErrors(nIdx) = norm(fVals - fValsApprox) / norm(fVals);
    end
    writematrix(gridErrors, sprintf('../results/adr_grid_s_%d_n_1_%d.csv', s, n));
    fourierErrors(nIdx) = f.relError(fApprox);
    writematrix(gridErrors, sprintf('../results/adr_fourier_s_%d_n_1_%d.csv', s, n));
end

% Plot error versus stamping level
if computeGridError
    semilogy(nVals, gridErrors, '-x', 'DisplayName', 'Monte-Carlo');
    hold on
end
semilogy(nVals, fourierErrors, '-o', 'DisplayName', 'exact');
hold on
legend('show', 'Interpreter', 'latex', 'Location', 'southwest');
xlabel('$N$ (stamping level)', 'Interpreter', 'latex')
ylabel('$\frac{\|f - f^{s, N}\|_{L^2}}{\|f\|_{L^2}}$',...
    'Interpreter', 'latex')
hold off

% Plot furthest stamped right hand sides
% Full grid
figure
gridN = 2^10;
x = (0:(gridN - 1)) ./ gridN;
[xx, yy, zz] = meshgrid(x);

subplot(1, 2, 1)
fVals = f.eval([xx(:), yy(:), zz(:)]);
v = reshape(fVals, size(xx));
writematrix(v, sprintf('../results/adr_f_gridN_%d_n_%d.csv', gridN, n));
xslice = [.4,.8, 1];    % location of y-z planes
yslice = 1;              % location of x-z plane
zslice = [0,0.5];         % location of x-y planes

slice(xx,yy,zz,v,xslice,yslice,zslice)
xlabel('$x_1$', 'Interpreter', 'latex');
ylabel('$x_2$', 'Interpreter', 'latex');
zlabel('$x_3$', 'Interpreter', 'latex');

subplot(1, 2, 2)
fRecoveredVals = real(fApproxHandle([xx(:), yy(:), zz(:)]));
v = reshape(fRecoveredVals, size(xx));
writematrix(v, sprintf('../results/adr_fapprox_gridN_%d_n_%d.csv', gridN, n));

slice(xx,yy,zz,v,xslice,yslice,zslice)
xlabel('$x_1$', 'Interpreter', 'latex');
ylabel('$x_2$', 'Interpreter', 'latex');
zlabel('$x_3$', 'Interpreter', 'latex');

set(figure(1),'renderer','painters')
exportgraphics(figure(1),...
    '../../Paper/Figures/ADR3DError.pdf', ...
    'Resolution',300)
set(figure(2),'renderer','painters')
exportgraphics(figure(2),...
    '../../Paper/Figures/ADR3DRightHandSide.pdf', ...
    'Resolution',300)
