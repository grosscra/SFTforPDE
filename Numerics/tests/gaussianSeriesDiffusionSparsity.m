addpath('../utils');

close all
rng(41);

N = 100; % Maximum bandwidth of data in any dimension
sVals = 2 .^ (1:6); % Sparsity of Fourier data
d = 2; % Dimensions of problem
nVals = 1:3; % Stamping levels

gridErrors = zeros(length(sVals), length(nVals));

% Keep diffusion coefficient Fourier coefficients constant
aNumTerms = 2;
aCoeff = 2 * rand(aNumTerms, 1) - 1;
while any(abs(aCoeff) < 1e-1)
    aCoeff = 2 * rand(aNumTerms, 1) - 1;
end

% Diffusion coefficient
aFreq = randi([1, N/2], aNumTerms, d) - floor(N / 4);
aRate = (1.1 ^ d) * ones(aNumTerms, 1); % TODO: Come up with better sparsity scheme
% Add constant term for ellipticity
a = gaussianSeries(aFreq, aCoeff, aRate) +  ceil(norm(aCoeff)); % Cheat for image

figure
% Plot fft
fftN = 2^11;
x = (0 : (fftN - 1)) ./ fftN;
[X, Y] = meshgrid(x);
x = [X(:), Y(:)];

y = a.eval(x);
y = reshape(y, [fftN, fftN]);

yHat = fftshift(fftshift((1 / fftN^2) * fft2(y), 1), 2);
surf(X * fftN - fftN/2, Y * fftN - fftN/2, 3 + abs(yHat), 'EdgeColor', 'none');
view(2);
xlabel('$k_1$', 'Interpreter','latex');
ylabel('$k_2$', 'Interpreter','latex');
xlim([-N/2, N/2])
ylim([-N/2, N/2])


% Add constant term for ellipticity
a = gaussianSeries(aFreq, aCoeff, aRate) + 4 * ceil(norm(aCoeff)); % Reset for test

figure

% Forcing function
fFreq = randi([1, N], 1, d) - floor(N / 2);
f = fourierSeriesSine(fFreq, 1);


for s_idx = 1:length(sVals)
    s = sVals(s_idx)
    
    diffApprox = sparseADRApproximate(d, s, N + 1, @f.eval, @a.eval, ...
        'aGrad', @a.evalGrad, 'sparseMatrix', true, 'sigma', .95, 'C', 10, ...
        'primeShift', 200, 'randomScale', 5);

    if d < 0
        % Uniform error evaluation grid (200 points in each dimension)
        grid = gridAsRowVectors(d, 200);
    else
        % Random grid over 1000 points total
        grid = rand(1000, d);
    end

    fVals = f.eval(grid);
    
    for n_idx = 1:length(nVals)
        n = nVals(n_idx)
        
        u = diffApprox.solve(n);
        fApprox = diffApprox.apply(u);
        fValsApprox = fApprox(grid);
        gridErrors(s_idx, n_idx) = norm(fVals - fValsApprox) / norm(fVals);
    end
    semilogy(nVals, gridErrors(s_idx, :), '-x', 'DisplayName', ...
        ['$s=', num2str(s), '$ Monte-Carlo']);
    hold on
end

legend('show', 'Interpreter', 'latex', 'Location', 'southwest');
xlabel('$N$ (stamping level)', 'Interpreter', 'latex')
xticks(nVals)
ylabel('$\frac{\|f - f^{s, N}\|_{L^2}}{\|f\|_{L^2}}$',...
    'Interpreter', 'latex')
hold off

% set(figure(1),'renderer','painters')
% exportgraphics(figure(1),...
%     '../../Paper/Figures/gaussianSeriesDiffusionSparsityFFT.pdf', ...
%     'Resolution',300)
% set(figure(2),'renderer','painters')
% exportgraphics(figure(2),...
%     '../../Paper/Figures/gaussianSeriesDiffusionSparsity.pdf', ...
%     'Resolution',300)