addpath('../utils');

rng(74);

N = 1000; % Maximum bandwidth of data in any dimension
s = 2; % Sparsity of Fourier data
dVals = 2 .^(0:2:10); % Dimensions of problem
nVals = 1:5; % Stamping levels

gridErrors = zeros(length(dVals), length(nVals));
fourierErrors = zeros(length(dVals), length(nVals));

% Keep diffusion coefficient Fourier coefficients constant
aNumTerms = 1;
aCoeff = 2 * rand(aNumTerms, 1) - 1;
while any(abs(aCoeff) < 1e-1)
    aCoeff = 2 * rand(aNumTerms, 1) - 1;
end


for d_idx = 1:length(dVals)
    d = dVals(d_idx)

    % Diffusion coefficient
    aFreq = randi([1, N], aNumTerms, d) - floor(N / 2);
    % Add constant term for ellipticity
    a = fourierSeriesCosine(aFreq, aCoeff) + 4 * ceil(norm(aCoeff));

    % Forcing function
    fFreq = randi([1, N], 1, d) - floor(N / 2);
    f = fourierSeriesSine(fFreq, 1);
    
    diff = sparseADR(f.freq, f.coeff, a.freq, a.coeff, ...
        'sparseMatrix', true);
    diffApprox = sparseADRApproximate(d, s, N + 1, @f.eval, @a.eval, ...
        'aGrad', @a.evalGrad, 'sparseMatrix', false);

    if d < 4
        % Uniform error evaluation grid (200 points in each dimension)
        grid = gridAsRowVectors(d, 200);
    else
        % Random grid over 200 points total
        grid = rand(200, d);
    end

    fVals = f.eval(grid);
    
    for n_idx = 1:length(nVals)
        n = nVals(n_idx)
        
        u = diffApprox.solve(n);
        fApprox = diff.applyToFourierSparse(u, n);
        fValsApprox = fApprox.eval(grid);
        gridErrors(d_idx, n_idx) = norm(fVals - fValsApprox) / norm(fVals);
        fourierErrors(d_idx, n_idx) = f.relError(fApprox);
    end
    semilogy(nVals, gridErrors(d_idx, :), '-x', 'DisplayName', ...
        ['$d=', num2str(d), '$ Monte-Carlo']);
    hold on
    semilogy(nVals, fourierErrors(d_idx, :), '-o', 'DisplayName', ...
        ['$d=', num2str(d), '$ exact']);
    hold on
end

legend('show', 'Interpreter', 'latex', 'Location', 'southwest');
xlabel('$N$ (stamping level)', 'Interpreter', 'latex')
xticks(nVals)
ylabel('$\frac{\|f - f^{s, N}\|_{L^2}}{\|f\|_{L^2}}$',...
    'Interpreter', 'latex')
hold off


% savefig("../results/oneTermDiffusion.fig")
%%
set(gcf,'renderer','painters')
exportgraphics(gcf,'../../Paper/Figures/oneTermDiffusion.pdf',...
    'Resolution',300)