addpath('../utils');

close all
rng(74);

N = 1000; % Maximum bandwidth of data in any dimension
s = 16; % Sparsity of Fourier data
dVals = 2 .^(1:4); % Dimensions of problem
nVals = 1:4; % Stamping levels

gridErrors = zeros(length(dVals), length(nVals));

% Keep diffusion coefficient Fourier coefficients constant
aNumTerms = 2;
aCoeff = 2 * rand(aNumTerms, 1) - 1;
while any(abs(aCoeff) < 1e-1)
    aCoeff = 2 * rand(aNumTerms, 1) - 1;
end


for d_idx = 1:length(dVals)
    d = dVals(d_idx)

    % Diffusion coefficient
    aFreq = randi([1, N/2], aNumTerms, d) - floor(N / 4);
    aRate = (1.1 ^ d) * ones(aNumTerms, 1); % TODO: Come up with better sparsity scheme
    % Add constant term for ellipticity
    a = gaussianSeries(aFreq, aCoeff, aRate) + 4 * ceil(norm(aCoeff));
    if d == 2
        a2 = gaussianSeries(aFreq, aCoeff, aRate) + 4 * ceil(norm(aCoeff));
    end
        

    % Forcing function
    fFreq = randi([1, N], 1, d) - floor(N / 2);
    f = fourierSeriesSine(fFreq, 1);
    
    diffApprox = sparseADRApproximate(d, s, N + 1, @f.eval, @a.eval, ...
        'aGrad', @a.evalGrad, 'sparseMatrix', true);

    if d < 0
        % Uniform error evaluation grid (200 points in each dimension)
        grid = gridAsRowVectors(d, 200);
    else
        % Random grid over 500 points total
        grid = rand(1000, d);
    end

    fVals = f.eval(grid);
    
    for n_idx = 1:length(nVals)
        n = nVals(n_idx)
        
        u = diffApprox.solve(n);
        fApprox = diffApprox.apply(u);
        fValsApprox = fApprox(grid);
        gridErrors(d_idx, n_idx) = norm(fVals - fValsApprox) / norm(fVals);
    end
    semilogy(nVals, gridErrors(d_idx, :), '-x', 'DisplayName', ...
        ['$d=', num2str(d), '$ Monte-Carlo']);
    hold on
end

legend('show', 'Interpreter', 'latex', 'Location', 'southwest');
xlabel('$N$ (stamping level)', 'Interpreter', 'latex')
xticks(nVals)
ylabel('$\frac{\|f - f^{s, N}\|_{L^2}}{\|f\|_{L^2}}$',...
    'Interpreter', 'latex')
hold off

savefig("../results/gaussianSeriesDiffusionStamp.fig")
set(gcf,'renderer','painters')
exportgraphics(gcf,...
    '../../Paper/Figures/gaussianSeriesDiffusionStamp.pdf', ...
    'Resolution',300)