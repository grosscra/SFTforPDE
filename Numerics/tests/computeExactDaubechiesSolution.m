% Diffusion coefficient (Daubechies et al., Testcase 2)
epsilon = 1/128;
a = @(x)(1/10) .* exp( ...
            (0.6 + 0.2 * cos(2 * pi * x)) ...
            ./ (1 + 0.7 * sin(2 * pi * x ./ epsilon)) ...
        );
aGrad = @(x)-(2 * pi) .* a(x) .*...
            ( ...
                0.2 * sin(2 * pi * x) .* ...
                    (1 + 0.7 * sin(2 * pi * x ./ epsilon)) + ...
                (0.7 / epsilon) * cos(2 * pi * x ./ epsilon) .* ...
                    (0.6 + 0.2 * cos(2 * pi * x)) ...
            ) ...
            ./ ((1 + 0.7 * sin(2 * pi * x ./ epsilon)) .^ 2);

% Forcing function (Daubechies et al., Testcase 2)
fStart = @(x)exp(-cos(2 * pi * x));
fMean = integral(fStart, 0, 1);
f = @(x)fStart(x) - fMean; % Forcing function must be mean-zero

% Compute "exact" solution by integrating

numGridPoints = 1000;
grid = (0:(numGridPoints - 1)).' ./ numGridPoints;

fName = ['../cache/nonsparse1d_N_', num2str(numGridPoints), '.mat'];
if exist(fName, 'file')
    load(fName)
else
    % tic
    % true = trueUnivariateSolution(a, f, grid);
    % toc
    tic
    trueSol = trueUnivariateSolutionParallel(a, f, grid);
    toc

    save(fName, 'trueSol');
end