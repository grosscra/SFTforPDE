addpath('../utils')

% Diffusion coefficient (Daubechies et al., Testcase 2)
epsilon = 1/128;
a = @(x)(1/10) .* exp( ...
            (0.6 + 0.2 * cos(2 * pi * x)) ...
            ./ (1 + 0.7 * sin(2 * pi * x ./ epsilon)) ...
        );
aGrad = @(x)-(2 * pi) .* a(x) .*...
            ( ...
                0.2 * sin(2 * pi * x) .* ...
                    (1 + 0.7 * sin(2 * pi * x ./ epsilon)) + ...
                (0.7 / epsilon) * cos(2 * pi * x ./ epsilon) .* ...
                    (0.6 + 0.2 * cos(2 * pi * x)) ...
            ) ...
            ./ ((1 + 0.7 * sin(2 * pi * x ./ epsilon)) .^ 2);

% Forcing function (Daubechies et al., Testcase 2)
fStart = @(x)exp(-cos(2 * pi * x));
fMean = integral(fStart, 0, 1);
f = @(x)fStart(x) - fMean; % Forcing function must be mean-zero

% Parameters
N = 1536; % Bandwidth
sVals = [4, 8, 12]; % Sparsity of data
d = 1;
nVals = 1:3;

% N = 5 * 1536;
% sVals = 1500;
% nVals = 1;

% Compute "exact" solution by integrating

numGridPoints = 10000;
grid = (0:(numGridPoints - 1)).' ./ numGridPoints;

fName = ['../cache/nonsparse1d_N_', num2str(numGridPoints), '.mat'];
if exist(fName, 'file')
    load(fName)
else
    % tic
    % true = trueUnivariateSolution(a, f, grid);
    % toc
    tic
    trueSol = trueUnivariateSolutionParallel(a, f, grid);
    toc

    save(fName, 'trueSol');
end

trueSol = trueSol - mean(trueSol);

% Precompute right-hand-side on grid

fVals = f(grid);

% Result arrays
gridErrors = zeros(length(sVals), length(nVals));
gridErrorsProxy = zeros(length(sVals), length(nVals));

for sIdx = 1:length(sVals)
    s = sVals(sIdx)

    diffApprox = sparseADRApproximate(1, s, N, f, a, 'aGrad', aGrad, ...
        'sparseMatrix', false);
%     disp(length(diffApprox.aFreq));

    for nIdx = 1:length(nVals)
        n = nVals(nIdx)

        u = diffApprox.solve(n);
        fApprox = diffApprox.apply(u);

        recoveredSol = u.eval(grid);
        gridErrors(sIdx, nIdx) = norm(recoveredSol - trueSol) ...
                                    / norm(trueSol);
        fValsApprox = fApprox(grid);
        gridErrorsProxy(sIdx, nIdx) = norm(fValsApprox - fVals) ...
                                        / norm(fVals);
    end
    semilogy(nVals, gridErrors(sIdx, :), '-x', 'DisplayName', ...
        ['$s=', num2str(s), '$ $L^2$ error']);
    hold on
    semilogy(nVals, gridErrorsProxy(sIdx, :), '-o', 'DisplayName', ...
        ['$s=', num2str(s), '$ proxy error']);
    hold on
end

legend('show', 'Interpreter', 'latex', 'Location', 'southwest');
xlabel('$n$ (stamping level)', 'Interpreter', 'latex')
ylabel('Relative error', 'Interpreter', 'latex')
hold off

% savefig("../results/Daubechies1D.fig")
% exportgraphics(gcf,'../../Paper/Figures/Daubechies1D.png','Resolution',300)