addpath('../utils');
N = 100; % Maximum bandwidth of data in any dimension
sVals = 2 .^ (1:6); % Sparsity of Fourier data
d = 2; % Dimensions of problem
nVals = 1:4; % Stamping levels

gridErrors = zeros(length(sVals), length(nVals));

% Keep diffusion coefficient Fourier coefficients constant
aNumTerms = 1;
aCoeff = 2 * rand(aNumTerms, 1) - 1;
while any(abs(aCoeff) < 1e-1)
    aCoeff = 2 * rand(aNumTerms, 1) - 1;
end

% Diffusion coefficient
aFreq = randi([1, N], aNumTerms, d) - floor(N / 2);
aRate = (1.1 ^ d) * ones(aNumTerms, 1); % TODO: Come up with better sparsity scheme
% Add constant term for ellipticity
a = gaussianSeries(aFreq, aCoeff, aRate) + 4 * ceil(norm(aCoeff));

% Plot fft
figure
fftN = 2^10;
x = (0 : (fftN - 1)) ./ fftN;
[X, Y] = meshgrid(x);
x = [X(:), Y(:)];

y = a.eval(x);
y = reshape(y, [fftN, fftN]);

yHat = fftshift(fftshift((1 / fftN^2) * fft2(y), 1), 2);

% subplot(2, 1, 1)
% mesh(abs(yHat))
% tol = 1e-5;
% title(['DFT: $N = ', num2str(fftN), ', s \approx ', num2str(nnz(abs(yHat) > tol)), '$'], 'Interpreter', 'latex') 
% subplot(2, 1, 2)
% % y = fftshift(fftshift(y, 1), 2);
% mesh(X, Y, y)
% title(['Signal: periodicity $\approx ', num2str(abs(y(1) - y(end))), '$'], 'Interpreter', 'latex')

figure

% Forcing function
fFreq = randi([1, N], 1, d) - floor(N / 2);
f = fourierSeriesSine(fFreq, 1);


for s_idx = 1:length(sVals)
    s = sVals(s_idx);
    
    diffApprox = sparseADRApproximate(d, s, N + 1, @f.eval, @a.eval, ...
        'aGrad', @a.evalGrad, 'sparseMatrix', true, 'sigma', .1);

    if d < 4
        % Uniform error evaluation grid (200 points in each dimension)
        grid = gridAsRowVectors(d, 200);
    else
        % Random grid over 200 points total
        grid = rand(200, d);
    end

    fVals = f.eval(grid);
    
    for n_idx = 1:length(nVals)
        n = nVals(n_idx)
        
        u = diffApprox.solve(n);
        fApprox = diffApprox.apply(u);
        fValsApprox = fApprox(grid);
        gridErrors(s_idx, n_idx) = norm(fVals - fValsApprox) / norm(fVals);
    end
    semilogy(nVals, gridErrors(s_idx, :), '-x', 'DisplayName', ...
        ['$s=', num2str(s), '$']);
    hold on
end

legend('show', 'Interpreter', 'latex', 'Location', 'southwest');
xlabel('$n$ (stamping level)', 'Interpreter', 'latex')
ylabel('Proxy for $\frac{\|u - u^{n,s}\|_{H^1}}{\|u\|_{H^1}}$',...
    'Interpreter', 'latex')
hold off


savefig("../results/oneGaussianTermDiffusionStampSparsity.fig")
exportgraphics(gcf,...
    '../../Paper/Figures/oneGaussianTermDiffusionStampSparsity.png', ...
    'Resolution',300)