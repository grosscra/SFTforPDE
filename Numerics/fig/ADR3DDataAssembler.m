addpath('../utils')

computeGridError = true;
nVals = 1:2;
gridErrors = readmatrix('../results/adr_grid_s_10_n_1_2.csv');
fourierErrors = readmatrix('../results/adr_fourier_s_10_n_1_2.csv');
% Plot error versus stamping level
if computeGridError
    semilogy(nVals, gridErrors, '-x', 'DisplayName', 'Monte-Carlo');
    hold on
end
semilogy(nVals, fourierErrors, '-o', 'DisplayName', 'exact');
hold on
legend('show', 'Interpreter', 'latex', 'Location', 'southwest');
xlabel('$N$ (stamping level)', 'Interpreter', 'latex')
xticks(nVals)
ylabel('$\frac{\|f - f^{s, N}\|_{L^2}}{\|f\|_{L^2}}$',...
    'Interpreter', 'latex')
hold off

set(figure(1),'renderer','painters')
exportgraphics(figure(1),...
    '../../Paper/Figures/ADR3DError.pdf', ...
    'Resolution',300)