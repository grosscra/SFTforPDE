% Creates stamp data to be used in TikZ
addpath('../utils');
aFreq = ...
    [1, 2;
    -2, 1];
[aFreq, aCoeff] = realFreqCoeff(aFreq, true);
fFreq = ...
    [3, -2];
[fFreq, fCoeff] = realFreqCoeff(fFreq, false);
diff = sparseADR(fFreq, fCoeff, aFreq, aCoeff);

fname = '../../Paper/Figures/stamp.csv';

N = 5;
currentFreq = fFreq;
writelines("set,x,y,", fname, 'WriteMode', 'overwrite')
printFreq(currentFreq, 0, fname, 'append');
for n = 1:N
    nextStamp = diff.stamp(n);
    newFreq = setdiff(nextStamp, currentFreq, 'rows');
    printFreq(newFreq, n, fname, 'append');
    currentFreq = nextStamp;
end

function [freq, coeff] = realFreqCoeff(complexFreq, meanTerm)
% Mirror complex frequencies to create real-valued Fourier frequencies
% Optionally, add an all zero frequency if the function has a mean
% Also return a dummy set of all-one coefficients
    d = size(complexFreq, 2);
    freq = union(complexFreq, -complexFreq, 'rows');
    if meanTerm
        freq = union(zeros(1, d), freq, 'rows');
    end
    coeff = ones([length(freq), 1]);
end

function printFreq(freq, n, fname, writeMode)
% Write the frequencies in a stamp level, tagged with that level n
    taggedFreq = [n * ones(size(freq, 1), 1), freq];
    writematrix(taggedFreq, fname, 'WriteMode', writeMode);
end