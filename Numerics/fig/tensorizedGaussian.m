addpath('../utils/')

modulation = [3 2; -5 15];
coeff = [1; 1];
rate = [0.5; 2];
g = gaussianSeries(modulation, coeff, rate);

N = 100;
x = (1:(N)) ./ N;
[xx, yy] = meshgrid(x);

z = g.eval([xx(:), yy(:)]);
zz = reshape(z, size(xx));
zzCentered = fftshift(fftshift(zz, 1), 2);

fnameBase = sprintf('gaussian_xN_%d_yN_%d.csv', N, N);
fname = fullfile('..', '..', 'Paper', 'Figures', fnameBase);
writelines('x,y,z', fname, 'WriteMode', 'overwrite');
writematrix([xx(:) - 0.5, yy(:) - 0.5, zzCentered(:)], ...
    fname, 'WriteMode', 'append');

zzHat = abs(fftshift(fftshift(fft2(zz), 1), 2));
xft = -(N/2 - 1):(N/2);
[xxft, yyft] = meshgrid(xft);

fnameBase = sprintf('gaussian_FFT_xN_%d_yN_%d.csv', N, N);
fname = fullfile('..', '..', 'Paper', 'Figures', fnameBase);
writelines('x,y,z', fname, 'WriteMode', 'overwrite');
writematrix([xxft(:), yyft(:), zzHat(:)], ...
    fname, 'WriteMode', 'append');

% 
% t = tiledlayout(1, 2,'Padding','tight');
% t.Units = 'inches';
% t.OuterPosition = [0.25 0.25 5 3];
% 
% nexttile;
% mesh(xx - 0.5, yy - 0.5, zzCentered);
% xlabel('$x_1$', 'Interpreter','latex');
% ylabel('$x_2$', 'Interpreter','latex');
% title('$c_1G_{r_1, \mathbf{k}_1} + c_2 G_{r_2, \mathbf{k}_2}$', 'Interpreter','latex')
% 
zzHat = abs(fftshift(fftshift(fft2(zz), 1), 2));
% nexttile;
% surf(xx * N - N/2, yy * N - N/2, zzHat, 'EdgeColor', 'none');
% view(2);
% xlabel('$k_1$', 'Interpreter','latex');
% ylabel('$k_2$', 'Interpreter','latex');
% title('$c_1\hat{G}_{r_1, \mathbf{k}_1} + c_2 \hat{G}_{r_2, \mathbf{k}_2}$', 'Interpreter','latex')
% 
% set(gcf,'renderer','painters')
% exportgraphics(gcf, '../../Paper/Figures/gaussianExample.pdf', 'Resolution', 300);