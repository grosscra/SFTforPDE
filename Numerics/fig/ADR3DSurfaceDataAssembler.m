function ADR3DSurfaceDataAssembler
    % Dummy function for data sharing to local functions
    file = '../../Paper/Results/ADR3D/ADRsurfaces.mat';
    data = load(file, 'fVals', 'fRecoveredVals');
    fVals = data.fVals;
    fRecoveredVals = data.fRecoveredVals;
    % Defines fVals and fRecoveredVals
    gridN = length(fVals);
    
    subsampleFactor = 2^0;
    fVals = fVals(1:subsampleFactor:end, 1:subsampleFactor:end,...
        1:subsampleFactor:end);
    length(fVals)
    gridN = gridN / subsampleFactor;
    x = (0:(gridN - 1)) ./ gridN;
    [xx, yy, zz] = meshgrid(x);
    
%     xslice = [x(length(x) / 2), x(end)];   
%     yslice = [x(length(x) / 2), x(end)];
%     zslice = 0;
    
    % slice(xx, yy, zz, fVals, xslice, yslice, zslice, 'cubic');
    % shading flat;
    
    oneFourthIndex = ceil(length(x) / 4);
    oneHalfIndex = 2 * oneFourthIndex;
    threeFourthsIndex = 3 * oneFourthIndex;
    writeSlice('x', x(oneHalfIndex));
    writeSlice('x', x(threeFourthsIndex));
    writeSlice('x', x(end));
    
    writeSlice('y', x(end));

    writeSlice('z', x(1));
    writeSlice('z', x(oneHalfIndex));
    
    function writeSlice(var, slice)
        % Write the data masked to the slice where var is constant
        % var should be a character: 'x', 'y' or 'z'.
        % 
        parent = fullfile('..', '..', 'Paper', 'Figures');
        fValsFName = fullfile(parent, sprintf('fVals_N%d_%s%f.csv', ...
            gridN, var, slice));
        fRecoveredValsFName= fullfile(parent, ...
            sprintf('fRecoveredVals_N%d_%s%f.csv', gridN, var, slice));
        if var == 'x'
            mask = xx == slice;
        elseif var == 'y'
            mask = yy == slice;
        else
            mask = zz == slice;
        end
        writelines('x,y,z,c', fValsFName);
        writematrix([xx(mask), yy(mask), zz(mask), fVals(mask)], fValsFName,...
            'WriteMode', 'append');
        writelines('x,y,z,c', fRecoveredValsFName);
        writematrix([xx(mask), yy(mask), zz(mask), fRecoveredVals(mask)], ...
            fRecoveredValsFName, 'WriteMode', 'append');
    end
end