sVals = [2, 5];
resultsDir = fullfile("..", "..", "Paper", "Results", "ADR3D");
varNames = {'n', 'err'};
varTypes = {'int32', 'double'};
final = [];
opts = delimitedTextImportOptions("VariableNames", varNames, ...
    "VariableTypes", varTypes);
for s = sVals
    tail = sprintf("_s%d_n_y.csv", s);
    fourierError = readtable( ...
        fullfile(resultsDir, sprintf("fourierErrors%s", tail)), ...
        opts, "ReadVariableNames", false);
    gridError = readtable( ...
        fullfile(resultsDir, sprintf("gridErrors%s", tail)), ...
        opts, "ReadVariableNames", false);
    curr = join(fourierError, gridError, 'Keys', 'n');
    curr = addvars(curr, s * ones([height(curr), 1]), ...
        'Before', 'n','NewVariableNames',{'s'});
    if istable(final)
        final = union(final, curr);
    else
        final = curr;
    end
end
resultFile = fullfile("..", "..", "Paper", "Figures", "ADR3DError.csv");
writetable(final, resultFile);