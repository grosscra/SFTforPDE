files = split(ls('../results/*.csv'));
files = sort(files(1:end-1));
files = files([6, 2, 7, 3, 8, 4, 5, 1])
results = cell(length(files), 1);
figure
for i = 1:length(files)
    results{i} = readmatrix(files{i});
    filedata = split(files{i}, '_');
    namedata = split(filedata{1}, '/');
    d = filedata{3};
    if strcmp(d, '1')
        continue
    end
    if strcmp(namedata{3}, 'grid')
        linespec = '-x';
        errorType = 'Monte-Carlo';
    else
        linespec = '-o';
        errorType = 'exact';
    end
    semilogy(results{i}(1, :), results{i}(2, :), linespec, ...
        'DisplayName', sprintf('$d=%s$ %s', d, errorType));
    hold on
end
legend('show', 'Interpreter', 'latex', 'Location', 'southwest');
xlabel('$N$ (stamping level)', 'Interpreter', 'latex')
xticks(results{1}(1, :));
ylabel('$\frac{\|f - f^{s, N}\|_{L^2}}{\|f\|_{L^2}}$',...
    'Interpreter', 'latex')
hold off

%%
set(gcf,'renderer','painters')
exportgraphics(gcf,'../../Paper/Figures/diffusionHighSparsity.pdf',...
    'Resolution',300)