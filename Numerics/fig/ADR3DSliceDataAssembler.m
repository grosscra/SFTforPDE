function ADR3DSliceDataAssembler
    % Dummy function for data sharing to local functions
    s = 10;
    for n = 1:2
        file = sprintf('../../Paper/Results/ADR3D/ADRsurfaces_n%d_s%d.mat', n, s);
        data = load(file, 'fVals', 'fRecoveredVals');
        fVals = data.fVals;
        fRecoveredVals = data.fRecoveredVals;
        % Defines fVals and fRecoveredVals
        gridN = length(fVals);
        
        subsampleFactor = 2^0;
        fVals = fVals(1:subsampleFactor:end, 1:subsampleFactor:end,...
            1:subsampleFactor:end);
        fVals = real(fVals);
        fRecoveredVals = fRecoveredVals(1:subsampleFactor:end, ...
            1:subsampleFactor:end, 1:subsampleFactor:end);
        fRecoveredVals = real(fRecoveredVals);
    
        length(fVals)
        gridN = gridN / subsampleFactor;
        x = (0:(gridN - 1)) ./ gridN;
        [xx, yy, zz] = meshgrid(x);
        
    %     xslice = [x(length(x) / 2), x(end)];   
    %     yslice = [x(length(x) / 2), x(end)];
    %     zslice = 0;
        
        % slice(xx, yy, zz, fVals, xslice, yslice, zslice, 'cubic');
        % shading flat;
        
        oneFourthIndex = ceil(length(x) / 4);
        oneHalfIndex = 2 * oneFourthIndex;
        threeFourthsIndex = 3 * oneFourthIndex;
        writeSlice('x', x(oneHalfIndex));
        writeSlice('x', x(threeFourthsIndex));
        writeSlice('x', x(end));
        
        writeSlice('y', x(end));
    
        writeSlice('z', x(1));
        writeSlice('z', x(oneHalfIndex));
    end
    
    function writeSlice(var, slice)
        % Write the data masked to the slice where var is constant
        % var should be a character: 'x', 'y' or 'z'.
        % 
        parent = fullfile('..', '..', 'Paper', 'Figures');
        fValsFName = fullfile(parent, sprintf('fVals_N%d_%s%f_2d.csv', ...
            gridN, var, slice));
        fRecoveredValsFName= fullfile(parent, ...
            sprintf('fRecoveredVals_N%d_n%d_s%d_%s%f_2d.csv', gridN, n, s, var, slice));
        if var == 'x'
            mask = xx == slice & yy < 0.5 & zz < 0.5;
            newx = yy;
            newy = zz;
        elseif var == 'y'
            mask = yy == slice;
            newx = xx;
            newy = zz;
        else
            mask = zz == slice;
            newx = xx;
            newy = yy;
        end
        writelines('x,y,c', fValsFName);
        writematrix([newx(mask), newy(mask), fVals(mask)], fValsFName,...
            'WriteMode', 'append');
        writelines('x,y,c', fRecoveredValsFName);
        writematrix([newx(mask), newy(mask), fRecoveredVals(mask)], ...
            fRecoveredValsFName, 'WriteMode', 'append');
    end
end